# Bioleaching plant remote monitoring

Repository of the ["Bioleaching plant remote monitoring" bachelor's thesis](https://upcommons.upc.edu/handle/2117/355945) (in catalan).

## Files

This repository consists of:

- `doc/` contains the source files for the thesis pdf document.
- `src/app/` is the folder for the phone application. Written in Java using [Android Studio](https://developer.android.com/studio).
- `src/server/` has the server software Python files. It was developed using the [Flask](https://flask.palletsprojects.com/) framework.
- `src/telegram/` contains scripts for the telegram bot, using [python-telegram-bot](https://docs.python-telegram-bot.org/en/stable/).
