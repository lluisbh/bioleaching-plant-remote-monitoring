\NeedsTeXFormat{LaTeX2e}[2018/12/01]
\ProvidesClass{tfgitic}[2019/10/23 iTIC-UPC Bachelor thesis format v6.3]
\LoadClass[a4paper,
           11pt,
           DIV=12,
           headings=big,
           twoside=semi,
           bibliography=totoc
           ]{scrbook}[2019/02/01]
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{environ,lmodern,newverbs,amsmath,amssymb}
\RequirePackage[automark,headsepline=0.4pt]{scrlayer-scrpage}
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{tikz,xcolor,booktabs}
\RequirePackage[
style=alphabetic,
datezeros=false]{biblatex}[2018/11/02]
\RequirePackage[
binary-units=true,
per-mode=symbol,
round-mode=places,
round-precision=2,
round-integer-to-decimal = false,
output-decimal-marker={,}]{siunitx}
\RequirePackage[autostyle=true]{csquotes}

% Les opcions de la classe
\newif\if@creativecommons  \@creativecommonstrue
\DeclareOption{creativecommons}{\@creativecommonstrue}
\DeclareOption{nocreativecommons}{\@creativecommonsfalse}
\newif\if@catalanlang  \@catalanlangtrue
\DeclareOption{catalan}{\@catalanlangtrue}
\DeclareOption{english}{\@catalanlangfalse}
\ProcessOptions\relax

\if@catalanlang
\PassOptionsToPackage{english,main=catalan}{babel}
\else
\PassOptionsToPackage{main=english,catalan}{babel}
\fi
\RequirePackage{babel}


% Configuració de les cometes
\DeclareQuoteAlias{spanish/spanish}{catalan}
\MakeAutoQuote{«}{»}


% El color principal, Pantone 3005
\definecolor{upcblue}{HTML}{007AC9}

\DeclareFixedFont{\helvbupc}{T1}{phv}{bx}{n}{2cm} 
\DeclareFixedFont{\smallhelvbupc}{T1}{phv}{bx}{n}{1.8cm} 
\DeclareFixedFont{\helvupc}{T1}{phv}{m}{n}{2cm} 

% bola upc de 10cm
\def\bola@upc{
    \fill [fill=upcblue]  (0,0) circle (5);
    \foreach \x in {-1.8,0,1.8}
    \foreach \y in {-1.8,0,1.8}
    {
      \fill [fill=white,yshift=1cm] (\x,\y) circle (.7);
    }
    \draw [color=white,text centered,font=\helvbupc,yshift=0.2cm] 
      (-1.8,-3  ) node {U}
      (0,-3)      node {P}
      (1.8,-3)    node {C};
}

% bola upc amb les lletres centrades a sota
\def\bola@upc@text{
  \bola@upc
  \draw [color=upcblue,text centered,font=\helvupc] 
      (0,-7) node {UNIVERSITAT POLIT\`ECNICA DE CATALUNYA};
}

% comanda
\newcommand{\bolaupctext}[1][1cm]{\resizebox{!}{#1}{\tikz\bola@upc@text;}}


%% Portada ah-hoc
\renewcommand*{\maketitle}{%
  \begin{titlepage}
    \begin{center}
      \bolaupctext[1.5cm]
      \par\bigskip
      \textsf{\color{upcblue}\LARGE Escola Politècnica Superior d'Enginyeria de Manresa}
    \end{center}
    \vfill
    \begin{center}
    \rule{0.9\textwidth}{1pt}\par
    \noindent\Huge\textbf{\@title}\par
    \ifx\@subtitle\@empty\else\bigskip\noindent\LARGE\@subtitle\par\fi
    \bigskip\bigskip
    \Large\today\par
    \rule{0.9\textwidth}{1pt}\par      
    \end{center}

    \vfill

    \begin{center}
      \LARGE
      {\scriptsize %
        \iflanguage{catalan}{treball de fi de grau que presenta}{}
        \iflanguage{english}{bachelor's thesis submmited by}{}
      }\\
      \textsc{\@author}\\
      {\scriptsize
        \iflanguage{catalan}{en compliment dels requisits per assolir
          el}{}
        \iflanguage{english}{in partial fulfillment of the
          requirements for the}{}
      }\\
      \iflanguage{catalan}{\textsc{Grau d'Enginyeria en Sistemes TIC}}{}
      \iflanguage{english}{\textsc{Degree of ICT Systems Engineering}}{}
      \par
      \bigskip\Large
      {
        \iflanguage{catalan}{Direcció: }{}
        \iflanguage{english}{Advisor: }{}
        \@advisor
      }\\
    \end{center}

    \newpage\thispagestyle{empty}
    
    \if@creativecommons
    \begin{minipage}{1.0\linewidth}
      \iflanguage{english}{
        \large This work is licensed under the Creative Commons
        Attribution-NonCommercial-ShareAlike 3.0 Spain License. To view
        a copy of this license, visit
        \url{http://creativecommons.org/licenses/by-nc-sa/3.0/es} or send a
        letter to Creative Commons, PO Box 1866, Mountain View, CA
        94042, USA.}{}
      \iflanguage{catalan}{
        \large Aquesta obra està subjecta a una llicència
        Attribution-NonCommercial-ShareAlike 3.0 Spain de Creative
        Commons. Per veure'n una còpia, visiteu
        \url{https://creativecommons.org/licenses/by-nc-sa/3.0/es/deed.ca}
        o envieu una carta a Creative Commons, 171 Second Street, Suite
        300, San Francisco, California 94105, USA.}{}
    \end{minipage}
    \fi
    
    \vfill
  \end{titlepage}  
  %
  \ifx\@dedication\empty\else
  \begin{titlepage}
    \hbox{}
    \vskip 3cm
    \begin{raggedleft}
      \large\@dedication\par
    \end{raggedleft}
    \vfill
  \end{titlepage}
  \fi
  %
  \ifx\ackmsg\empty\else
  \begin{titlepage}
    \hbox{}
    \vskip 3cm
    \begin{center}
      \Large
      \iflanguage{english}{\textbf{Acknowledgments}}{}
      \iflanguage{catalan}{\textbf{Agraïments}}{}
    \end{center}
    \medskip
    \ackmsg
  \end{titlepage}
  \fi
}%


\newcommand{\advisor}[1]{\def\@advisor{#1}}
\newcommand{\commitee}[1]{}


% estil de pagina
\pagestyle{scrheadings}
\setkomafont{pageheadfoot}{\normalfont\rmfamily\itshape\scriptsize}
\setkomafont{pagenumber}{\normalfont\rmfamily\itshape\normalsize}


% enumeracions amb parèntesi (només en català)
\iflanguage{catalan}{
  \renewcommand{\labelenumi}{\arabic{enumi})}
  \renewcommand{\labelenumii}{\alph{enumii})}
  \renewcommand{\labelenumiii}{\roman{enumiii})}
}

\newcommand{\acro}[1]{\textsc{#1}}
\newcommand{\est}[1]{\textit{#1}}
\let\fitx\nolinkurl
\newverbcommand{\ord}{}{}

% Abstracts en català i anglès
\newsavebox{\abstractca}
\newsavebox{\abstracten}

\newenvironment{resum}{%
  \lrbox{\abstractca}
  \begin{minipage}{1.0\linewidth}
    \setlength{\parindent}{1em}
}{
  \end{minipage}
  \endlrbox
}

\newenvironment{summary}{%
  \lrbox{\abstracten}
  \begin{minipage}{1.0\linewidth}
    \setlength{\parindent}{1em}
}{
  \end{minipage}
  \endlrbox
}


% Agraïments
\def\ackmsg{}
\NewEnviron{acknowledgments}{\xdef\ackmsg{\unexpanded\expandafter{\BODY}}}



% Preferències pels floats
\def\fps@figure{tbp}
\def\fps@table{tbp}

% Fonts especials
\setkomafont{descriptionlabel}{\sffamily}
\setkomafont{caption}{\small}
\setkomafont{captionlabel}{\usekomafont{caption}}

\AtBeginDocument{
  \sloppy
  \maketitle{}
  \frontmatter
  \iflanguage{catalan}{
    \chapter{Resum}
    \unhbox\abstractca
    {\let\clearpage\relax\chapter{Abstract}}
    \unhbox\abstracten      
  }{}
  \iflanguage{english}{
    \chapter{Abstract}
    \unhbox\abstracten      
    {\let\clearpage\relax\chapter{Resum}}
    \unhbox\abstractca
  }{}
  \tableofcontents
  \mainmatter
}

