from flask import (
    Blueprint, g, request, session, url_for, abort, current_app
)
from bioapp import db, models, msgmanager
from bioapp.models import Register, Component, Alert
from bioapp.alerts import *
from bioapp.acts import *

from gevent import spawn, GreenletExit, sleep
from gevent.select import select
from gevent.queue import Queue
import json
import os

from bioapp.messagemanager import MessageManagerFull

bp = Blueprint('ws', __name__, url_prefix='/ws')

def is_socket(socket):
    if socket is None:
        abort(404)

def socket_send(socket, msg):
    """
    Make sure that the socket is not closed before sending
    """
    if not socket.closed:
        socket.send(msg)

@bp.route('/echo')
def echo(socket = None):
    is_socket(socket)
    while not socket.closed:
        message = socket.receive()
        socket_send(socket, message)

def info_receive(queue, socket):
    """
    Function that reads the incoming messages (jsons) of the websocket
    """
    try:
        llista_r = [socket.stream.handler.rfile,]
        while True:
            try:
                sleep(0.1) #Espera per buidar tot el socket d'entrada
                rlist, _, _ = select(llista_r, [], [])
            except ValueError:
                queue.put(StopIteration)
                return
            
            for r in rlist:
                if r == llista_r[0]:
                    queue.put(("RECIEVE",))
    except GreenletExit:
        return
    
def info_send(queue):
    """
    Listens for incoming messages (in form of AsyncResult in gevent)
    """
    try:
        try:
            asnc = msgmanager.add_box()
        except MessageManagerFull:
            #close socket
            queue.put(("END", ''))
            return
        
        
        while True:
            msg = asnc.get()
            queue.put(("SEND", json.dumps(msg)))
    except GreenletExit:
        msgmanager.remove_box(asnc)
        
@bp.route('/info')
def info(socket = None):
    is_socket(socket)
    while not socket.closed:
        #queue per on es rebran les instruccions a fer d'aquest greenlet
        q = Queue(maxsize=5)
        #obrir greenlet per escoltar enviament de missatges
        green_send = spawn(info_send, queue=q)
        #obrir greenlet per rebre missatges
        green_receive = spawn(info_receive, queue=q, socket=socket)
        
        #llegir de la llista fins que s'acabi la comunicació
        for action in q:
            if action[0] == "SEND":
                if socket.closed: #socket is closed
                    break
                socket.send(action[1])
                
            elif action[0] == "RECIEVE":
                msg = socket.receive()
                #reenviar pel socket per notificar que s'ha rebut
                #socket.send(msg)
                
                if msg is None: #socket is closed
                    break

                #guardar fitxer per saber que s'ha rebut
                with open(os.path.join(current_app.instance_path, 'last_received.txt'), 'w') as f:
                    f.write(msg+'\n')
                
                try:
                    json_send = json.loads(msg)
                except json.JSONDecodeError:
                    continue
                
                #guardar a la bbdd
                db_save(json_send)
            elif action[0] == "END":
                break
        
        #Forçar acabament dels greenlets oberts
        green_send.kill(block=False)
        green_receive.kill(block=False)

def db_save(json):
    """
    Guarda els continguts del json en la bbdd
    """
    for i in json:
        try:
            node = i["id"]
            value = i["value"]
        except TypeError:
            continue

        if node == 36: #no guardar! col·lisió amb colors
            continue
        
        c = Component.query.filter(Component.id == node).first()
        if c is not None:
            r = Register.query.filter(Register.component == c).order_by(Register.date.desc()).first()
        
            if r is None or r.value != value:
                r = Register(value = value, component = c)
                db.session.add(r)

                id_act(c, r)
            
            check_alerts(c, r)

    db.session.commit()

ACTS = {13 : color_lix, 23 : color_rec}

def id_act(component, register):
    """
    Funció per detectar casos especials en que els valors s'han de tractar
    """
    if component.id in ACTS:
        ACTS[component.id](component, register)
    

ALERTS = {3 : pH_alert, 12 : pH_alert, \
          31 : hlvt_tank2_alert, \
          29 : llvt_tank1_alert, \
          7 : hlvt_bio_alert}

def check_alerts(component, register):
    """
    Afegeix una alerta si és necessari
    """
    if component.id in ALERTS:
        #no més d'una alerta per un component cada 12 hores
        if Alert.query.filter(Alert.component == component, Alert.date >= (default_date() - 720)).first() is not None:
            return
        
        #crida la funció
        a = ALERTS[component.id](component, register)
        if a is not None:
            db.session.add(a)
