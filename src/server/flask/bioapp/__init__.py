import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_sockets import Sockets
from flask_login import LoginManager

from bioapp.messagemanager import MessageManager

#per funcionar amb gevent
from gevent import monkey
monkey.patch_all()

#objecte per controlar la BBDD
db = SQLAlchemy()
#objecte per controlar Websockets
sockets = Sockets()
#objecte gestor de la sessió
login_manager = LoginManager()

#objecte per comunicar-se amb els websockets
msgmanager = MessageManager()

def create_app(test_config=None):
    """
    Crea i configura l'aplicació bioapp
    """

    app = Flask(__name__, instance_relative_config = True)
    app.config.from_mapping(
        #Clau secreta de l'aplicació
        #es sobreescriu per la instància de la app
        #es conserva al fer proves
        SECRET_KEY = 'prova',
        #fitxer de base de dades
        #DATABASE=os.path.join(app.instance_path, "bioapp.db"),
        SQLALCHEMY_DATABASE_URI = "sqlite:///" +
        os.path.join(app.instance_path, "bioapp.db"),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
    )

    if test_config is None:
        #Si el fitxer existeix, afegeix la seva configuració
        #app.config.from_pyfile("config.py", silent=True)
        pass
    else:
        #carrega la configuració de proves
        app.config.from_pyfile(test_config)

    try:
        #Crea la carpeta d'instància si no existeix
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route("/hello/")
    def hello():
        return "Hello World!"

    #@app.route("/wstest/")
    #def wstest():
    #     import random
    #     send = {"id":random.randint(1, 10), "value": bool(random.randint(0,1))}
    #     msgmanager.signal(send)
    #     
    #     return "Information sent to websocket"

    #inicialitza bd per funcionar amb aquesta app
    #with app.app_context():
    db.init_app(app)
    #inicialitza websocket
    sockets.init_app(app)
    #Inicialitza login
    login_manager.init_app(app)
    
    #Blueprint d'autentificació
    from . import auth
    app.register_blueprint(auth.bp)

    #Blueprint de websocket
    #Es registra a sockets, no app
    from . import ws
    sockets.register_blueprint(ws.bp)

    #Blueprint de websocket per proves
    from . import ws_test
    sockets.register_blueprint(ws_test.bp)

    #Blueprint de dades
    from . import data
    app.register_blueprint(data.bp)

    #Interfície d'usuaris
    from . import users
    app.register_blueprint(users.bp)

    #Stations/Components
    from . import elements
    app.register_blueprint(elements.bp)

    return app
