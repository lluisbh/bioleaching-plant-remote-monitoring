from bioapp import db
from bioapp.models import Register, Component

def color_separate(value):
    blue = (value & 0xFF)
    value = value >> 8
    green = (value & 0xFF)
    value = value >> 8
    red = (value & 0xFF)
    print(red, green, blue)
    return red, green, blue

def color_lix(component, register):
    red, green, blue = color_separate(int(register.value))
    r = Register(value = red, component = Component.query.filter(Component.id == 36).first())
    db.session.add(r)
    r = Register(value = green, component = Component.query.filter(Component.id == 37).first())
    db.session.add(r)
    r = Register(value = blue, component = Component.query.filter(Component.id == 38).first())
    db.session.add(r)

def color_rec(component, register):
    red, green, blue = color_separate(int(register.value))
    r = Register(value = red, component = Component.query.filter(Component.id == 39).first())
    db.session.add(r)
    r = Register(value = green, component = Component.query.filter(Component.id == 40).first())
    db.session.add(r)
    r = Register(value = blue, component = Component.query.filter(Component.id == 41).first())
    db.session.add(r)
