from gevent.event import AsyncResult

class MessageManagerFull(Exception):
    def __init__(self, message="The box list is full"):
        self.message = message
        super().__init__(self.message)


class MessageManager(object):
    def __init__(self, max_box = 1):
        self._boxes = []
        self._boxes_avaliable = max_box

    def add_box(self):
        #if self._boxes_avaliable <= 0:
        #    raise(MessageManagerFull)
        #self._boxes_avaliable -= 1
        asnc = MessageBox()
        self._boxes.append(asnc)
        return asnc

    def remove_box(self, box):
        self._boxes.remove(box)

    def signal(self, sendval):
        for b in self._boxes:
            b.set(sendval)
        
class MessageBox(object):
    def __init__(self):
        self._async = AsyncResult()

    def get(self):
        ret = self._async.get()
        self._async = AsyncResult()
        return ret

    def set(self, val):
        self._async.set(val)
