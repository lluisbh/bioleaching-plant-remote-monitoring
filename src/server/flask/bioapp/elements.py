from flask import (
    Blueprint, request, jsonify, abort, url_for
)

from bioapp import models, db

from flask_login import login_required

import json

bp = Blueprint('elements', __name__, url_prefix='/elements')

@bp.route('/stations/', methods = ('GET',))
@login_required
def stations():
    stations = models.Station.query.all()

    ret = []
    
    for s in stations:
        d = {"Station" : s.id,
             "Url" : url_for("elements.stations_id", id_sta=str(s.id))}
        ret.append(d)
    return jsonify(ret)

@bp.route('/stations/<id_sta>/', methods = ('GET',))
@login_required
def stations_id(id_sta):
    try:
        id = int(id_sta)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)
    
    station = models.Station.query.filter(models.Station.id == id).first()

    if station is None:
        abort(404)

    ret = {"Id" : station.id,
           "Name" : station.name,
           "Components" : url_for("elements.stations_comp", id_sta=str(id))}
    
    return jsonify(ret)

@bp.route('/stations/<id_sta>/components/', methods = ('GET',))
@login_required
def stations_comp(id_sta):
    try:
        id = int(id_sta)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)
    
    station = models.Station.query.filter(models.Station.id == id).first()

    if station is None:
        abort(404)

    ret = []
    
    for c in station.components:
        d = {"Component" : c.id,
             "Url" : url_for("elements.components_id", id_com=str(c.id))}
        ret.append(d)        
    return jsonify(ret)

@bp.route('/components/', methods = ('GET',))
@login_required
def components():
    components = models.Component.query.all()

    ret = []
    
    for c in components:
        d = {"Component" : c.id,
             "Url" : url_for("elements.components_id", id_com=str(c.id))}
        ret.append(d)
    return jsonify(ret)

@bp.route('/components/<id_com>/', methods = ('GET',))
@login_required
def components_id(id_com):
    try:
        id = int(id_com)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)
    
    component = models.Component.query.filter(models.Component.id == id).first()

    if component is None:
        abort(404)

    ret = {"Id" : component.id,
           "Name" : component.name,
           "Description" : component.description,
           "Act" : component.can_act,
           "Stations" : url_for("elements.components_stat", id_com=str(id))}
    if not component.unit is None:
        ret["Unit"] = component.unit
    if not component.max_value is None:
        ret["MaxValue"] = component.max_value
    if not component.min_value is None:
        ret["MinValue"] = component.min_value
    
    return jsonify(ret)

@bp.route('/components/<id_com>/stations/', methods = ('GET',))
@login_required
def components_stat(id_com):
    try:
        id = int(id_com)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)
    
    component = models.Component.query.filter(models.Component.id == id).first()

    if component is None:
        abort(404)

    ret = []
    
    for s in component.stations:
        d = {"Station" : s.id,
             "Url" : url_for("elements.stations_id", id_sta=str(s.id))}
        ret.append(d)        
    return jsonify(ret)
