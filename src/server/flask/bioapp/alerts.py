from bioapp import db
from bioapp.models import Alert, Register, default_date

def pH_alert(component, register):
    MESSAGE = "pH fora de rang [1.6, 1.8]"

    if register.value < 1.6 or register.value > 1.8:
        date_from = default_date() - 10
        registers = Register.query.filter(Register.component==component, date_from < Register.date).order_by(Register.date.asc())
        #primer registre
        registre_first = Register.query.filter(Register.component==component, date_from >= Register.date).order_by(Register.date.desc()).first()
        if registre_first is not None:
            if registre_first.value < 1.6:
                for r in registers:
                    if r.value > 1.6:
                        return None
                return Alert(component = component, message = MESSAGE)
            if registre_first.value > 1.8:
                for r in registers:
                    if r.value < 1.8:
                        return None
                return Alert(component = component, message = MESSAGE)
    return None

def hlvt_tank2_alert(component, register):
    if register.value > 0:
        return Alert(component = component, message = "Nivell alt activat, s'ha aturat la planta.")
    return None

def llvt_tank1_alert(component, register):
    if register.value > 0:
        return Alert(component = component, message = "Nivell baix activat.")
    return None

def hlvt_bio_alert(component, register):
    MESSAGE = "Nivell alt activat durant molt de temps."

    if register.value > 0:
        date_from = default_date() - 10
        registers = Register.query.filter(Register.component==component, date_from < Register.date).order_by(Register.date.asc())
        #primer registre
        registre_first = Register.query.filter(Register.component==component, date_from >= Register.date).order_by(Register.date.desc()).first()
        if registre_first is not None and registre_first.value > 0:
            for r in registers:
                if r.value == 0:
                    return None
            return Alert(component = component, message = MESSAGE)
    return None
