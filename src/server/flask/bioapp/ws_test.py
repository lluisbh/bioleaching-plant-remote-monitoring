"""
Canal de proves per websocket
"""

from flask import (
    Blueprint, g, request, session, url_for, abort, current_app
)

bp = Blueprint('ws_test', __name__, url_prefix='/ws_test')

def is_socket(socket):
    if socket is None:
        abort(404)

def socket_send(socket, msg):
    """
    Make sure that the socket is not closed before sending
    """
    if not socket.closed:
        socket.send(msg)

@bp.route('/echo')
def echo(socket = None):
    is_socket(socket)
    while not socket.closed:
        message = socket.receive()
        socket_send(socket, message)
