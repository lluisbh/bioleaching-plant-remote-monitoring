from flask import (
    Blueprint, request, jsonify, abort, url_for
)

from bioapp import db

from bioapp.models import User, Admin

from flask_login import current_user, login_required

import json
import secrets

bp = Blueprint('users', __name__, url_prefix='/users')

@bp.route('/', methods = ('GET', 'POST'))
@login_required
def users():
    if (current_user.is_admin()):
        if request.method == 'GET':
            return users_get()
        elif request.method == 'POST':
            return users_post()
    abort(403)

def users_get():
    ret = [{"username" : u.username} for u in User.query.all()]
    return jsonify(ret)

def users_post():
    try:
        data = json.loads(request.data)
    except json.decoder.JSONDecodeError:
        abort(400)
    
    try:
        username = data["username"]
    except KeyError:
        abort(400)
    except ValueError:
        abort(400)

    if username == "":
        abort(400)
        
    if User.query.filter(User.username == username).first() is not None:
        abort(405)
    
    try:
        isadmin = bool(data["admin"])
        if "password" in data:
            password = data["password"]
        else:
            alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$%"
            password = ''.join(secrets.choice(alphabet) for i in range(16))
    except KeyError:
        abort(400)
    except ValueError:
        abort(400)

    if isadmin:
        u = Admin(username = username)
    else:
        u = User(username = username)
    u.set_password(password)
    db.session.add(u)
    db.session.commit()
    
    return jsonify({"admin" : isadmin, "username" : username, "password" : password}), 201


@bp.route('/<id_user>', methods = ('PUT', 'DELETE', ))
@login_required
def user_id(id_user):
    if (current_user.is_admin()):
        if request.method == 'PUT':
            return user_id_put(id_user)
        elif request.method == 'DELETE':
            return user_id_delete(id_user)
    abort(403)

def user_id_put(id_user):
    user = User.query.filter(User.username == id_user).first()
    if user is not None:
        try:
            data = json.loads(request.data)
        except json.decoder.JSONDecodeError:
            abort(400)

        if "password" in data:
            password = data["password"]
            if password == "":
                abort(400)
            user.set_password(password)

            db.session.add(user)
            db.session.commit()

            return ('', 204)
        abort(400)
    abort(404)
    
    
def user_id_delete(id_user):
    user = User.query.filter(User.username == id_user).first()
    if user is not None:
        
        db.session.delete(user)
        db.session.commit()

        return ('', 204)
    abort(404)

