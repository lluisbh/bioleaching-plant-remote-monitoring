from werkzeug.security import generate_password_hash, check_password_hash

from bioapp import db
from time import time

from sqlalchemy import exc

import importlib.resources
import xml.etree.ElementTree as ET

class User(db.Model):
    """
    Representa un usuari de l'aplicació, amb el seu nom i contrasenya.
    
    Variables:
    - username: Nom d'usuari, únic.
    - password: resum i sal de la contrasenya, ha de ser guardada amb la funció set_password pel seu correcte funcionament.

    Funcions:
    - set_password(self, password): Encripta i guarda la contrasenya.
    - check_password(self, password): Retorna si la contrasenya donada coincideix amb la guardada a la taula.
    """
    __tablename__ = "user"
    username = db.Column(db.String(16), primary_key=True)
    password = db.Column(db.String(120)) #revisar mida

    #polimorfisme
    type = db.Column(db.String(5))

    __mapper_args__ = {
        'polymorphic_identity':'user',
        'polymorphic_on':type
    }

    def set_password(self, password):
        self.password = generate_password_hash(password, salt_length=32)

    def check_password(self, password):
        return check_password_hash(self.password, password)

class Admin(User, db.Model):
    """
    Representa un usuari que és administrador.
    """
    __tablename__ = "admin"
    username = db.Column(db.String(16), db.ForeignKey('user.username'), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity':'admin'
    }

def default_date():
    return int(time()//60)+1

class Register(db.Model):
    """
    Valor guardat provinent de la planta.

    Variables:
    - value: Valor guardat, número de punt flotant.
    - date: Temps en minuts en que es va registrar el valor, numerat des de epoch.
    - component: Component a qui li pertany.
        * Claus: component_id.
    """

    id = db.Column(db.Integer, primary_key = True)
    value = db.Column(db.Float, nullable = False)
    date = db.Column(db.Integer, default=default_date)
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'), nullable = False)
    component = db.relationship("Component", foreign_keys=[component_id])

association_table = db.Table('stationcomponent', db.Model.metadata,
                          db.Column('left_id', db.Integer, db.ForeignKey('component.id')),
                          db.Column('right_id', db.Integer, db.ForeignKey('station.id'))
                          )

class Component(db.Model):
    """
    Sensor o actuador del qual es registren valors.
    
    Variables:
    - id: Número identificatiu del component.
    - name: Nom del component.
    - description: Descripió del component.
    - can_act: Defineix si s'accepta enviar commandes a la planta sobre aquest component.
    - value_type: Tipus de valor
    """
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), nullable = False)
    description = db.Column(db.Text(), nullable = False)
    unit = db.Column(db.String(5), nullable = True)
    can_act = db.Column(db.Boolean(), nullable = False)
    value_type = db.Column(db.Integer(), nullable = False)
    max_value = db.Column(db.Float(), nullable = True)
    min_value = db.Column(db.Float(), nullable = True)


class Station(db.Model):
    """
    Estació de la planta.

    Variables:
    - id: Número identificatiu de la estació.
    - name: Nom de la estació.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), nullable = False)
    components = db.relationship("Component",
                                 secondary=association_table,
                                 backref="stations")


class Alert(db.Model):
    """
    Alertes sobre determinats registres. Es generen quan es rep informació de la planta i es detecta una increpància.
    
    Variables:
    - id: Número identificatiu de la alerta.
    - component: Component a qui pertany.
        * Claus: component_id.
    - date: Temps en minuts en que es va registrar el valor, numerat des de epoch.
    - message: Text explicatiu sobre l'alerta.
    """
    id = db.Column(db.Integer, primary_key=True)
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'), nullable = False)
    component = db.relationship("Component", foreign_keys=[component_id])
    date = db.Column(db.Integer, default=default_date)
    message = db.Column(db.Text(), nullable=False)
    

def create_components():
    """
    Funció per crear els components per defecte. Llegeix del fitxer.
    """

    Station.query.delete()
    Component.query.delete()
    
    with importlib.resources.path("bioapp", 'components.xml') as p:
        tree_names = ET.parse(p)
        root_names = tree_names.getroot()
    
    with importlib.resources.path("bioapp", 'stations_elements.xml') as p:
        tree = ET.parse(p)
        root = tree.getroot()

    component_types = {"real" : 1, "int" : 2, "bool" : 3}

    dict_component = {}
    dict_station = {}
    dict_compstation = {}
    for station in root:
        name_id = station.attrib["name"]
        for s in root_names: 
            if name_id ==  s.attrib["name"]:
                id_station = int(station.attrib["id"])
                name = s.find("name").text
                class_station = Station(id = id_station, name = name)
                db.session.add(class_station)
                dict_station[id_station] = class_station
                dict_compstation[id_station] = []
                break
        else:
            continue
        
        for c in station:
            type = c.attrib["type"]
            id = int(c.attrib["id"])
            if type != "duplicate":
                act = c.attrib["can_act"] == "true"
                value_type = component_types[type]
                name_id = c.attrib["name"]
                try:
                    max_value = c.attrib["max"]
                except KeyError:
                    max_value = None
                    min_value = None
                else:
                    if max_value == "":
                        max_value = None
                    min_value = c.attrib["min"]
                    if min_value == "":
                        min_value = None
                for s in root_names: 
                    if name_id ==  s.attrib["name"]:
                        name = s.find("name").text
                        desc = s.find("description").text
                        if desc is None:
                            desc = ""
                        unit = s.find("unit").text
                        if unit == "":
                            unit = None
                        class_component = Component(id=id, name=name, description=desc, can_act=act,
                                                    value_type=value_type, max_value=max_value,
                                                    min_value=min_value, unit=unit)
                        
                        db.session.add(class_component)
                        dict_component[id] = class_component
                        break
            #if not id in dict_compstation[id_station]:
            dict_compstation[id_station].append(id)
    
    for station_id in dict_compstation:
        for component_id in dict_compstation[station_id]:
            dict_station[station_id].components.append(dict_component[component_id])

    db.session.commit()


def restart_database():
    """
    Reiniciar la base de dades per actualitzar-la. Conserva els usuaris i administradors.
    """

    admins = []
    users = []

    for u in Admin.query.all():
        admins.append((u.username, u.password))
    
    for u in User.query.all():
        for a in admins:
            if a[0] == u.username:
                break
        else:
            users.append((u.username, u.password))
        

    db.drop_all()
    db.create_all()
    create_components()
    
    for u in admins:
        db.session.add(Admin(username=u[0], password=u[1]))

    for u in users:
        try:
            db.session.add(User(username=u[0], password=u[1]))
        except exc.IntegrityError:
            pass

    db.session.commit()
