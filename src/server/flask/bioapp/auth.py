import functools

from flask import (
    Blueprint, g, request, session, url_for, abort
)
from bioapp import models, login_manager
from flask_login import UserMixin, current_user, login_user, logout_user

class UserLogin(UserMixin):
    def __init__(self, name):
        self.id = name

    def is_admin(self):
        return not (models.Admin.query.filter_by(username = self.id).one_or_none() is None)

@login_manager.user_loader
def load_user(username):
    if models.User.query.filter_by(username = username).one_or_none():
        return(UserLogin(username))
    return None

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/login', methods = ('POST',))
def login():
    if request.method == 'POST':
        response = {"response": False, "admin" : False}
        username = request.form['username']
        password = request.form['password']

        user_db = models.User.query.filter_by(username = username).one_or_none()
        user = load_user(username)
        if user is None:
            return response
        if user_db.check_password(password):
            response["response"] = True
        login_user(user, remember=True)
        if not user.is_admin():
            return response
        response["admin"] = True
        return response
    abort(405)

@bp.route('/logout', methods = ("PUT",))
def logout():
    logout_user()
    return ('', 204)
