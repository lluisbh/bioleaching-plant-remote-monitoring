from flask import (
    Blueprint, request, jsonify, abort, url_for
)

from bioapp import models, db, msgmanager

from flask_login import current_user, login_required

import json

bp = Blueprint('data', __name__, url_prefix='/data')

@bp.route('/', methods = ('GET',))
@login_required
def data():
    components = models.Register.query.with_entities(models.Register.component_id).distinct()#registres amb valors

    ret = []
        
    for c in components:
        d = {"Component" : c.component_id,\
             "Url" : url_for("data.data_id", id_req=str(c.component_id))}
        ret.append(d)
    return jsonify(ret)


@bp.route('/<id_req>', methods = ('GET', 'PUT'))
@login_required
def data_id(id_req):
    if request.method == 'GET':
        return data_id_get(id_req)
    elif request.method == 'PUT':
        return data_id_put(id_req)

def data_id_get(id_req):
    date_from_req = request.args.get('from')
    date_to_req = request.args.get('to')

    first_previous = request.args.get('firstprev') or 'true'
    
    try:
        date_from = int(date_from_req)
        date_to = int(date_to_req)
        id = int(id_req)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)
        
    registers = models.Register.query.filter(models.Register.component_id==id, date_from <= models.Register.date, date_to >= models.Register.date).order_by(models.Register.date.asc())
    #primer registre
    registre_first = models.Register.query.filter(models.Register.component_id==id, date_from > models.Register.date).order_by(models.Register.date.desc()).first()

    ret = []
        
    if first_previous != 'false' and registre_first is not None and (registers.count() == 0 or registers[0].date != date_from):
        d = {"Component" : registre_first.component_id,\
             "Date" : registre_first.date,\
             "Value" : registre_first.value}
        ret.append(d)
    
    for r in registers:
        d = {"Component" : r.component_id,\
             "Date" : r.date,\
             "Value" : r.value}
        ret.append(d)
    return jsonify(ret)

def data_id_put(id_req):
    if (current_user.is_admin()):
        try:
            data = json.loads(request.data)
        except json.decoder.JSONDecodeError:
            abort(400)
        try:
            id = int(id_req)
            command = data["command"]
        except KeyError:
            abort(400)
        except ValueError:
            abort(400)

        if not (isinstance(command, bool) or isinstance(command, int)) or not isinstance(id, int):
            abort(400)

        q =  models.Component.query.filter(models.Component.id == id).first()
        
        if q == None or not q.can_act:
            abort(400)
            
        sendJSON = {"id" : id, "value" : command}
        msgmanager.signal(sendJSON)
        
        return ('', 204)
                
    abort(403)


@bp.route('/alerts/', methods = ('GET',))
@login_required
def alerts():
    date_from_req = request.args.get('from')
    date_to_req = request.args.get('to')
    
    try:
        date_from = int(date_from_req)
        date_to = int(date_to_req)
    except TypeError:
        abort(400)
    except ValueError:
        abort(400)

    alerts = models.Alert.query.\
        filter(date_from <= models.Alert.date, date_to >= models.Alert.date).\
        order_by(models.Alert.date.asc())

    ret = []
    for a in alerts:
        d = {"Component" : a.component_id,\
             "Date" : a.date,\
             "Message" : a.message}
        ret.append(d)

    return jsonify(ret)

