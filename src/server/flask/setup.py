from setuptools import find_packages, setup

setup(
    name='bioapp',
    version='0.2.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask==1.1.2',
        'Werkzeug==1.0.1',
        'flask-sqlalchemy',
        'flask-sockets',
        'flask-login',
        'gevent',
    ],
)
