import sqlite3

class DatabaseManager():
    FILE = 'database.db'
    STATE_WAITING, STATE_CANCELLED, STATE_REGISTERED, STATE_ADMIN = range(4)
    
    def __init__(self):
        self.__con = sqlite3.connect(self.FILE)
        self.__create_tables()

    def close(self):
        self.__con.close()

    def __create_tables(self):
        cur = self.__con.cursor()
        cur.execute("""
        CREATE TABLE IF NOT EXISTS users (
        CHAT INT PRIMARY KEY NOT NULL,
        USERNAME TEXT,
        STATE INT
        )
        """)
        self.__con.commit()

    def __execute(self, command, parameters):
        cur = self.__con.cursor()
        #try:
        cur.execute(command, parameters)
        #except sqlite3.Error:
        #    pass
        #finally:
        cur.close()

    def __execute_return_all(self, command, parameters):
        cur = self.__con.cursor()
        #try:
        cur.execute(command, parameters)
        value = cur.fetchall()
        #except sqlite3.Error:
        #    value = []
        #finally:
        cur.close()
        return value

    def __execute_return_one(self, command, parameters):
        cur = self.__con.cursor()
        #try:
        cur.execute(command, parameters)
        value = cur.fetchone()
        #except sqlite3.Error:
        #    value = None
        #finally:
        cur.close()
        return value

    def __set_level(self, level, chat_id):
        self.__execute("UPDATE users SET STATE = ? WHERE chat = ?;", (level, chat_id))
        self.__con.commit()

    def __add_user(self, chat_id, name, level):
        self.__execute("INSERT INTO users (CHAT, USERNAME, STATE) VALUES (?, ?, ?);", (chat_id, name, level))
        self.__con.commit()

    def __delete_user(self, chat_id):
        self.__execute("DELETE FROM users WHERE chat = ?;", (chat_id, ))
        self.__con.commit()

    def set_user_admin(self, chat_id):
        self.__set_level(self.STATE_ADMIN, chat_id)
        self.__con.commit()

    def user_register(self, chat_id):
        self.__set_level(self.STATE_REGISTERED, chat_id)
        self.__con.commit()

    def user_cancel(self, chat_id):
        self.__set_level(self.STATE_CANCELLED, chat_id)
        self.__con.commit()

    def add_waiting(self, name, chat_id):
        self.__add_user(chat_id, name, self.STATE_WAITING)
        self.__con.commit()
    
    def user_remove(self, chat_id):
        self.__delete_user(chat_id)
        self.__con.commit()

    def admin_remove(self, chat_id):
        self.__set_level(self.STATE_REGISTERED, chat_id)
        self.__con.commit()

    def __get_users_by_state(self, state):
        return self.__execute_return_all("SELECT * FROM users WHERE state = ?;", (state,))

    def get_admins(self):
        return self.__get_users_by_state(self.STATE_ADMIN)

    def get_registered(self):
        return self.__get_users_by_state(self.STATE_REGISTERED)

    def get_user_by_name(self, name):
        return self.__execute_return_one("SELECT * FROM users WHERE username = ? LIMIT 1;", (name,))
    
    def get_user_by_chat(self, chat):
        return self.__execute_return_one("SELECT * FROM users WHERE chat = ? LIMIT 1;", (chat,))

    def get_waiting(self):
        return self.__get_users_by_state(self.STATE_WAITING)

    def get_valid(self):
        return self.__execute_return_all("SELECT * FROM users;", ())

    def user_status_exists(self, chat_id, state):
        v = self.__execute_return_one("SELECT Count(chat) FROM users WHERE chat = ? AND state = ?;", (chat_id, state))[0]
        return v != 0;

    def is_admin(self, chat_id):
        return self.user_status_exists(chat_id, self.STATE_ADMIN)

    def is_registered(self, chat_id):
        return self.user_status_exists(chat_id, self.STATE_ADMIN) or self.user_status_exists(chat_id, self.STATE_REGISTERED)

    def is_waiting(self, chat_id):
        return self.user_status_exists(chat_id, self.STATE_WAITING) or self.user_status_exists(chat_id, self.STATE_CANCELLED)

    def is_new(self, chat_id):
        return self.get_user_by_chat(chat_id) is None
