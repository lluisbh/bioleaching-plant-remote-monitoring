from alerts_pooling import *
from commands import add_handlers
from database_manager import DatabaseManager

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.error import Unauthorized

from time import sleep
import logging
import traceback

def notify_all(text):
    """
    Funció per enviar un missatge a tots els xats especificats.
    """
    dbm = DatabaseManager()
    for i in dbm.get_valid():
        try:
            bot.send_message(chat_id=i[0], text=text)
        except Unauthorized:
            dbm.user_remove(i[0])
    dbm.close()

def alert_callback(comp, stat, date, message):
    notify_all(f"ALERTA DEL SERVIDOR\n\nComponent: {comp}\nStation: {stat}\nData: {date}\n{message}")

if __name__ == '__main__':
    ######################
    # Guardar token de Telegram a un fitxer anomenat 'token'
    ######################
    with open('token', 'r') as token_file:
        updater = Updater(token=token_file.read(), use_context=True)
    dispatcher = updater.dispatcher
    bot = updater.bot
    
    #Errors
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    
    #Afegir accions al bot
    add_handlers(dispatcher)

    #notify_all("Hola món!")
    
    updater.start_polling()
    
    while True:
        try:
            alerts = AlertsPooling(alert_callback)
            alerts.run()
        except AlertConnectionException:
            try:
                print("No s'ha pogut realitzar la connexió al servidor")
                traceback.print_exc()
                sleep(10)
                print("Reintentant...")
            except KeyboardInterrupt:
                break
        except Exception as e:
            try:
                print("Error desconegut!")
                traceback.print_exc()
                sleep(10)
            except KeyboardInterrupt:
                break
        else:
            break
    
    updater.stop()
