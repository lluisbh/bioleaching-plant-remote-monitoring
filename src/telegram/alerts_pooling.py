import time
import random
import datetime

import requests

class AlertConnectionException(Exception):
    pass

class AlertsPooling:
    URL = "https://biometallum.epsem.upc.edu"
    def __init__(self, callback_on_alert):
        self.__callback_on_alert = callback_on_alert
        self.__minute = int(time.time() // 60)
        self.login()

    def login(self):
        username = "telegram_bot"
        password = "1IL3BM0a39AxDxdR"

        header = {"Content-Type" : "application/x-www-form-urlencoded"}
        
        response = requests.post(self.URL+'/auth/login', data=f"username={username}&password={password}", headers=header)

        if not response.ok:
            raise AlertConnectionException
        
        response_json = response.json()

        if not response_json["response"]:
            raise AlertConnectionException
        self.__cookies = response.cookies;

    def call(self):
        minute = int(time.time() // 60)
        response = requests.get(self.URL+'/data/alerts/', params={"from" : self.__minute, "to" : minute}, cookies=self.__cookies)

        if not response.ok:
            if response.status_code == 401:
                self.login()
                return
            print(response.status_code)
            raise AlertConnectionException

        self.__minute = minute+1
        
        response_json = response.json()

        for r in response_json:
            dt = datetime.datetime.fromtimestamp(r["Date"]*60)
            date_string = dt.strftime("%Y-%m-%d %H:%M")
            component, station = self.__get_elements(r["Component"])
            self.__callback_on_alert(component, station, date_string, r["Message"])

    def __get_elements(self, comp):
        comp_str = str(comp)

        response = requests.get(self.URL+f'/elements/components/{comp_str}/', cookies=self.__cookies)

        if not response.ok:
            if response.status_code == 401:
                self.login()
            return comp_str, ""

        response_json = response.json()
        name_component = response_json["Name"]
        
        response = requests.get(self.URL+f'/elements/components/{comp_str}/stations/', cookies=self.__cookies)

        if not response.ok:
            if response.status_code == 401:
                self.login()
            return comp_str, ""

        response_json = response.json()
        
        response = requests.get(self.URL+response_json[0]["Url"], cookies=self.__cookies)

        if not response.ok:
            if response.status_code == 401:
                self.login()
            return comp_str, ""

        response_json = response.json()
        name_station = response_json["Name"]

        return name_component, name_station
                
        
    def run(self):
        while (True):
            try:
                self.call()
                current_second = time.time()
                current_minute = current_second // 60
                time.sleep(((current_minute+1)*60) - current_second + random.uniform(0, 5))
            except KeyboardInterrupt:
                break
