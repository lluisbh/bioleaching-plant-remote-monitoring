from telegram.ext import ConversationHandler, CommandHandler, MessageHandler, Filters
from telegram.error import Unauthorized
from telegram import ReplyKeyboardMarkup

from database_manager import DatabaseManager

#{1051709562 : "@lluisbh"}

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="""
    Aquest bot s'encarrega d'enviar les alertes de la planta als usuaris registrats.
    Escriu /help per veure totes les comandes.
    """)

def help_command(update, context):
    dbm = DatabaseManager()
    msg = """
Comandes:
    
* /start: missatge de benvinguda.
* /help: veure tots els comandaments disponibles.
* /status: el teu estat actual"""

    chat_id = update.effective_chat.id

    if dbm.is_admin(chat_id):
        msg += """
* /requests: acceptar una petició. Es mostra un teclat amb totes les peticions sense confirmar o denegar.
* /requests_remove: esborrar una petició.
* /remove_user: eliminar un usuari.
* /add_admin: dona permisos d’administrador a un usuari.
* /remove_admin: treure permisos d'un administrador.
        """
    elif dbm.is_registered(chat_id):
        msg += """
* /quit: deixar de rebre notificacions.
        """
    elif dbm.is_new(chat_id):
        msg += """
* /register: envia una petició d'accés. Només es pot demanar una vegada.
        """
    context.bot.send_message(chat_id=chat_id, text=msg)
    dbm.close()
    
    

#Funció per conèixer l'estat actual
def state(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    
    if dbm.is_admin(chat_id):
        context.bot.send_message(chat_id=chat_id, text="Estat: administrador.")
    elif dbm.is_registered(chat_id):
        context.bot.send_message(chat_id=chat_id, text="Estat: registrat.")
    elif dbm.is_waiting(chat_id):
        context.bot.send_message(chat_id=chat_id, text="Estat: esperant confirmació.")
    else:
        context.bot.send_message(chat_id=chat_id, text="Estat: no estàs registrat.")

    dbm.close()

#Enviar petició
def send_request(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    
    if dbm.is_waiting(chat_id):
        context.bot.send_message(chat_id=chat_id, text="Ja estàs esperant per la petició.")
        dbm.close()
        return
    if not dbm.is_new(chat_id):
        context.bot.send_message(chat_id=chat_id, text="Ja estàs registrat!")
        dbm.close()
        return

    user = update.message.from_user

    dbm.add_waiting(user.username, chat_id)
    context.bot.send_message(chat_id=chat_id, text="Petició enviada. Contacta amb un administrador per confirmar el teu compte.")

    dbm.close()

#Veure i confirmar peticions (ConversationHandler)
REQ_NAME = 0
def see_requests(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    
    if not dbm.is_admin(chat_id):
        dbm.close()
        return ConversationHandler.END

    reply_keyboard = [["/cancel"]]
    for u in dbm.get_waiting():
        reply_keyboard.append([f"{u[1]}, {u[0]}"])
    
    context.bot.send_message(chat_id=chat_id, text="Especifíca l'usuari.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    dbm.close()
    
    return REQ_NAME

def cancel_accept_request(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text="No s'ha confirmat cap usuari.")
    return ConversationHandler.END

def accept_request(update, context):
    chat_id = update.effective_chat.id
    text = update.message.text
    s = text.split(", ")
    
    if len(s) != 2:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    name = s[0]
    try:
        chat = int(s[1])
    except:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END
    
    dbm = DatabaseManager()
    
    u = dbm.get_user_by_chat(chat)

    if u is None or u[2] != DatabaseManager.STATE_WAITING:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    dbm.user_register(u[0])

    try:
        context.bot.send_message(chat_id=u[0], text="""
        T'han registrat!
        El bot tenviarà notificacions quan aparegui una alerta.
        Per desapuntar-te, escriu /quit
        """)
    except Unauthorized:
        context.bot.send_message(chat_id=chat_id, text=f"No s'ha pogut enviar un missatge a {name}. No s'ha registrat. Ha blocat al bot?")
        dbm.user_remove(u[0])
        
        dbm.close()
        return ConversationHandler.END
        
    context.bot.send_message(chat_id=chat_id, text=f"Usuari {name} registrat!")

    dbm.close()
    return ConversationHandler.END


#Bloquejar peticions (ConversationHandler)
def cancel_remove_request(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text="No s'ha bloquejat cap usuari.")
    return ConversationHandler.END

def remove_request(update, context):
    chat_id = update.effective_chat.id
    text = update.message.text
    s = text.split(", ")
    
    if len(s) != 2:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    name = s[0]
    try:
        chat = int(s[1])
    except:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END
    
    dbm = DatabaseManager()
    
    u = dbm.get_user_by_chat(chat)

    if u is None or u[2] != DatabaseManager.STATE_WAITING:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    dbm.user_cancel(u[0])

    context.bot.send_message(chat_id=chat_id, text=f"Usuari {name} bloquejat.")

    dbm.close()
    return ConversationHandler.END


#Veure i eliminar usuaris (ConversationHandler)
USER_NAME = 0
def see_users(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    
    if not dbm.is_admin(chat_id):
        dbm.close()
        return ConversationHandler.END

    reply_keyboard = [["/cancel"]]
    for u in dbm.get_registered():
        reply_keyboard.append([f"{u[1]}, {u[0]}"])
    
    context.bot.send_message(chat_id=chat_id, text="Especifíca l'usuari.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    dbm.close()
    
    return USER_NAME

def cancel_remove_user(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text="No s'ha eliminat cap usuari.")
    return ConversationHandler.END

def remove_user(update, context):
    chat_id = update.effective_chat.id
    text = update.message.text
    s = text.split(", ")
    
    if len(s) != 2:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    name = s[0]
    try:
        chat = int(s[1])
    except:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END
    
    dbm = DatabaseManager()
    
    u = dbm.get_user_by_chat(chat)

    if u is None or u[2] != DatabaseManager.STATE_REGISTERED:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    dbm.user_remove(u[0])

    context.bot.send_message(chat_id=chat_id, text=f"Usuari {name} eliminat.")

    dbm.close()
    return ConversationHandler.END


#Afegir permisos a usuaris (ConversationHandler)
def cancel_admin_user(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text="No s'han modificat els permisos.")
    return ConversationHandler.END

def admin_user(update, context):
    chat_id = update.effective_chat.id
    text = update.message.text
    s = text.split(", ")
    
    if len(s) != 2:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    name = s[0]
    try:
        chat = int(s[1])
    except:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END
    
    dbm = DatabaseManager()
    
    u = dbm.get_user_by_chat(chat)

    if u is None or u[2] != DatabaseManager.STATE_REGISTERED:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'usuari.")
        dbm.close()
        return ConversationHandler.END

    dbm.set_user_admin(u[0])

    context.bot.send_message(chat_id=chat_id, text=f"L'usuari {name} ara és administrador.")

    dbm.close()
    return ConversationHandler.END


#Veure i treure permisos a administradors (ConversationHandler)
ADMIN_NAME = 0
def see_admins(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    
    if not dbm.is_admin(chat_id):
        dbm.close()
        return ConversationHandler.END

    reply_keyboard = [["/cancel"]]
    for u in dbm.get_admins():
        if u[0] == chat_id:
            continue
        reply_keyboard.append([f"{u[1]}, {u[0]}"])
    
    context.bot.send_message(chat_id=chat_id, text="Especifíca l'administrador.",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    dbm.close()
    
    return ADMIN_NAME

def cancel_remove_admin(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text="No s'ha eliminat cap administrador.")
    return ConversationHandler.END

def remove_admin(update, context):
    chat_id = update.effective_chat.id
    text = update.message.text
    s = text.split(", ")
    
    if len(s) != 2:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'administrador.")
        dbm.close()
        return ConversationHandler.END

    name = s[0]
    try:
        chat = int(s[1])
    except:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'administrador.")
        dbm.close()
        return ConversationHandler.END
    
    dbm = DatabaseManager()
    
    u = dbm.get_user_by_chat(chat)

    if u is None or u[2] != DatabaseManager.STATE_ADMIN:
        context.bot.send_message(chat_id=chat_id, text="No s'ha trobat l'administrador.")
        dbm.close()
        return ConversationHandler.END

    dbm.admin_remove(u[0])

    context.bot.send_message(chat_id=chat_id, text=f"L'usuari {name} ja no disposa de permisos.")

    dbm.close()
    return ConversationHandler.END


#Borrar-se a si mateix (ConversationHandler)
REMOVE_CONF = 0
def remove_self(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    if not (dbm.is_registered(chat_id) or dbm.is_admin(chat_id)):
        dbm.close()
        return ConversationHandler.END

    context.bot.send_message(chat_id=chat_id, text="Vols deixar de rebre notificacions? Si les vols tornar a rebre, un administrador ha de tornar a confirmar el teu compte.",
        reply_markup=ReplyKeyboardMarkup([["/yes"], ["/no"]], one_time_keyboard=True))
    dbm.close()
    return REMOVE_CONF

def remove_self_confirm(update, context):
    chat_id = update.effective_chat.id
    dbm = DatabaseManager()
    dbm.user_remove(chat_id)

    context.bot.send_message(chat_id=chat_id, text=f"T'has esborrat del registre.")

    dbm.close()
    return ConversationHandler.END

def remove_self_cancel(update, context):
    chat_id = update.effective_chat.id
    context.bot.send_message(chat_id=chat_id, text=f"No t'has esborrat del registre.")
    return ConversationHandler.END


def add_handlers(dispatcher):
    handler = CommandHandler('start', start)
    dispatcher.add_handler(handler)

    handler = CommandHandler('status', state)
    dispatcher.add_handler(handler)

    handler = CommandHandler('register', send_request)
    dispatcher.add_handler(handler)

    handler = CommandHandler('help', help_command)
    dispatcher.add_handler(handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('requests', see_requests)],
        states={
            REQ_NAME: [MessageHandler(Filters.text & ~Filters.command, accept_request), CommandHandler('cancel', cancel_accept_request)],
        },
        fallbacks=[CommandHandler('cancel', cancel_accept_request)],
    )
    dispatcher.add_handler(conv_handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('requests_remove', see_requests)],
        states={
            REQ_NAME: [MessageHandler(Filters.text & ~Filters.command, remove_request), CommandHandler('cancel', cancel_remove_request)],
        },
        fallbacks=[CommandHandler('cancel', cancel_remove_request)],
    )
    dispatcher.add_handler(conv_handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('remove_user', see_users)],
        states={
            USER_NAME: [MessageHandler(Filters.text & ~Filters.command, remove_user), CommandHandler('cancel', cancel_remove_user)],
        },
        fallbacks=[CommandHandler('cancel', cancel_remove_user)],
    )
    dispatcher.add_handler(conv_handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('add_admin', see_users)],
        states={
            USER_NAME: [MessageHandler(Filters.text & ~Filters.command, admin_user), CommandHandler('cancel', cancel_admin_user)],
        },
        fallbacks=[CommandHandler('cancel', cancel_admin_user)],
    )
    dispatcher.add_handler(conv_handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('remove_admin', see_admins)],
        states={
            ADMIN_NAME: [MessageHandler(Filters.text & ~Filters.command, remove_admin), CommandHandler('cancel', cancel_remove_admin)],
        },
        fallbacks=[CommandHandler('cancel', cancel_remove_admin)],
    )
    dispatcher.add_handler(conv_handler)

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('quit', remove_self)],
        states={
            REMOVE_CONF: [CommandHandler('yes', remove_self_confirm), CommandHandler('no', remove_self_cancel)],
        },
        fallbacks=[CommandHandler('no', remove_self_cancel)],
    )
    dispatcher.add_handler(conv_handler)
