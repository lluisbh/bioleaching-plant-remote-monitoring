package com.example.appbio;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.multidex.MultiDexApplication;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;

public class App extends MultiDexApplication {
    //Classe de l'app, generada quan s'inicia
    public static final String serverAddress = "https://biometallum.epsem.upc.edu/";//"http://192.168.1.17:8000/";

    //cookies
    private static CookieStoreBioapp cookieStore;

    @Override
    public void onCreate() {
        super.onCreate();

        Context context = this;
        //Tenir cookies preparades
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(sharedPreferences);
        cookieStore = new CookieStoreBioapp(sharedPreferences);
        CookieManager cookieManager = new CookieManager(cookieStore, null);
        CookieHandler.setDefault(cookieManager);

        if (!sharedPreferences.getBoolean("db_init", false)) {
            //Inicialitzar bbdd
            BDRoom.initDB(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("db_init", true);
            editor.apply();
        } else if (sharedPreferencesManager.isLoggedIn()) {
            DataRequest.resetData(context);
        }
        //Alarm per rebre dades del servidor
        DataRequest.startAlarm(context);
    }

    public static void logIn(Context context) {
        Intent intent = new Intent(context, ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void logOut(Context context) {
        BDRoom bdRoom = BDRoom.getDatabase(context);
        BDRoom.databaseWriteExecutor.execute(() -> {
            //Borrar totes les dades per evitar que s'hi accedeixi
            bdRoom.registerDAO().deleteAll();
            bdRoom.rangeDAO().deleteAll();
            ServerRequest request = new ServerRequest(serverAddress + "auth/logout", "PUT");
            request.sendRequest();

            //borrar cookies
            try {
                URI uri = new URI(serverAddress);
                for (HttpCookie c:
                    cookieStore.get(uri)) {
                    cookieStore.remove(uri, c);
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        });

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);
        sharedPreferencesManager.setIsLoggedIn(false);
        sharedPreferencesManager.apply();

        //Desactiva alarma al reiniciar el dispositiu
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        logIn(context);
    }
}
