package com.example.appbio;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface BDAlertDAO {
    @Insert
    void insert(BDAlertEntity element);

    @Query("SELECT * FROM Alert ORDER BY date DESC")
    LiveData<BDAlertEntity[]> getAlerts();
}
