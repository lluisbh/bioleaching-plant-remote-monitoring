package com.example.appbio;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLStationParser {

    public static class Element {
        public final String name;
        public final int id;
        public final int elementClass;

        public static final int CLASS_STATION = 0;
        public static final int CLASS_COMPONENT = 1;

        public Element(String name, int id, int elementClass) {
            this.name = name;
            this.id = id;
            this.elementClass = elementClass;
        }
    }

    private static final String NAME_DUPLICATE = "duplicate";
    private static final String NAME_REAL = "real";
    private static final String NAME_INTEGER = "int";
    private static final String NAME_BOOLEAN = "bool";

    public static class Component extends Element {
        public final int type;
        public final int stationId;
        public final float min;
        public final float max;
        public final boolean canAct;

        public static final int TYPE_DUPLICATE = 0;
        public static final int TYPE_REAL = 1;
        public static final int TYPE_INTEGER = 2;
        public static final int TYPE_BOOLEAN = 3;

        public Component(String name, int id, int type, int stationId, boolean canAct) {
            super(name, id, Element.CLASS_COMPONENT);
            this.type = type;
            this.stationId = stationId;
            this.min = -1;
            this.max = -1;
            this.canAct = canAct;
        }

        public Component(String name, int id, int type, int stationId, boolean canAct, float min, float max) {
            super(name, id, Element.CLASS_COMPONENT);
            this.type = type;
            this.stationId = stationId;
            this.min = min;
            this.max = max;
            this.canAct = canAct;
        }
    }

    public static class Station extends Element {
        public Station(String name, int id) {
            super(name, id, Element.CLASS_STATION);
        }
    }

    public static List<Element> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(in, null);
            parser.nextTag();
            return readParser(parser);
        } finally {
            in.close();
        }
    }

    private static List<Element> readParser(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Element> elements = new ArrayList<>();
        int station = 0;
        parser.require(XmlPullParser.START_TAG, null, "plant");
        while (parser.next() != XmlPullParser.END_TAG || !parser.getName().equals("plant")) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("station")) {
                Station s = readStation(parser);
                elements.add(s);
                station = s.id;

            } else {
                elements.add(readComponent(parser, station));
            }
        }

        return elements;
    }

    private static Station readStation(XmlPullParser parser) {
        String name = parser.getAttributeValue(null, "name");
        int id = Integer.parseInt(parser.getAttributeValue(null, "id"));
        return new Station(name, id);
    }

    private static Component readComponent(XmlPullParser parser, int station) {
        String name = parser.getAttributeValue(null, "name");
        int id = Integer.parseInt(parser.getAttributeValue(null, "id"));
        int type;
        switch (parser.getAttributeValue(null, "type")) {
            case NAME_REAL: {
                type = Component.TYPE_REAL;

                String actString = parser.getAttributeValue(null, "can_act");
                boolean canAct;
                canAct = actString.equals("true");

                String minString = parser.getAttributeValue(null, "min");
                float min;
                if (minString.isEmpty()) {
                    min = -1;
                } else {
                    min = Float.parseFloat(minString);
                }

                String maxString = parser.getAttributeValue(null, "max");
                float max;
                if (maxString.isEmpty()) {
                    max = -1;
                } else {
                    max = Float.parseFloat(maxString);
                }
                return new Component(name, id, type, station, canAct, min, max);
            }
            case NAME_INTEGER: {
                type = Component.TYPE_INTEGER;

                String actString = parser.getAttributeValue(null, "can_act");
                boolean canAct;
                canAct = actString.equals("true");

                String minString = parser.getAttributeValue(null, "min");
                int min;
                if (minString.isEmpty()) {
                    min = -1;
                } else {
                    min = Integer.parseInt(minString);
                }

                String maxString = parser.getAttributeValue(null, "max");
                int max;
                if (maxString.isEmpty()) {
                    max = -1;
                } else {
                    max = Integer.parseInt(maxString);
                }
                return new Component(name, id, type, station, canAct, min, max);
            }
            case NAME_BOOLEAN: {
                type = Component.TYPE_BOOLEAN;

                String actString = parser.getAttributeValue(null, "can_act");
                boolean canAct;
                canAct = actString.equals("true");

                return new Component(name, id, type, station, canAct, -1, -1);
            }
            case NAME_DUPLICATE: {
                type = Component.TYPE_DUPLICATE;
                return new Component(name, id, type, station, false);
            }
            default: {
                return new Component(name, 0, 0, 0, false);
            }
        }
    }
}
