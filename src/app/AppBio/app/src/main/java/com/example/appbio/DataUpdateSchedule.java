package com.example.appbio;

import android.content.Context;
import android.util.Log;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class DataUpdateSchedule {

    public static final ScheduledExecutorService executorService =
            Executors.newSingleThreadScheduledExecutor();
    public static ScheduledFuture<?> scheduledFuture;

    private static class ScheduledAction implements Runnable {

        private final Context context;
        private final SharedPreferencesManager sharedPreferences;

        public ScheduledAction(Context context) {
            this.context = context;
            sharedPreferences = new SharedPreferencesManager(context);
        }

        @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
        @Override
        public void run() {
            if (sharedPreferences.isLoggedIn()) {
                try {
                    Thread.sleep((long) (Math.random() * 5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                long time = new Date().getTime()/60000;
                DataRequest.getData(time, time, -1, context);
            } else {
                final ScheduledFuture<?> lock = scheduledFuture;
                synchronized (lock) {
                    lock.cancel(false);
                }
            }
        }
    }

    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public static void startSchedule(Context context) {
        long time = new Date().getTime();
        long initTime = 60 - (time - (time/60000)*60000)/1000;
        ScheduledAction scheduledAction = new ScheduledAction(context);
        executorService.execute(scheduledAction);
        final ScheduledFuture<?> lock = scheduledFuture;
        if (lock != null) {
            synchronized (lock) {
                if (lock.isDone()) {
                    scheduledFuture = executorService.scheduleAtFixedRate(scheduledAction, initTime, 60, TimeUnit.SECONDS);
                }
            }
        } else {
            scheduledFuture = executorService.scheduleAtFixedRate(scheduledAction, initTime, 60, TimeUnit.SECONDS);
        }
    }
}
