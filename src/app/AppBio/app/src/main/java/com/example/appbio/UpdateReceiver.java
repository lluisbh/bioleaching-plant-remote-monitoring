package com.example.appbio;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;

public class UpdateReceiver extends BroadcastReceiver {

    private static final String EXTRA_REALTIME = "com.example.appbio.REALTIME";
    private static final String EXTRA_PREVIOUSTIME = "com.example.appbio.PREVIOUSTIME";

    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    @Override
    public void onReceive(Context context, Intent intent) {

        long now = new Date().getTime()/60000;
        long prev = intent.getLongExtra(EXTRA_PREVIOUSTIME, now);

        //Get necessary data
        DataRequest.RunnableGetAlerts run = DataRequest.getAlerts(context, prev, now);
        synchronized (run) {
            //Wait to see if the request was successful
            if (run.isRunning()) {
                try {
                    run.wait(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (run.isSuccessful()) {
                prev = now+1; //update time
            }
        }
        //Restart alarm
        startAlarm(context, intent.getLongExtra(EXTRA_REALTIME, -1), prev);
    }

    public static void startAlarm(Context context, long actual_time, long previous_date) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        CookieStore cookieStore = new CookieStoreBioapp(sharedPreferences);
        CookieManager cookieManager = new CookieManager(cookieStore, null);
        CookieHandler.setDefault(cookieManager);
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(sharedPreferences);

        if (sharedPreferencesManager.isLoggedIn()) {
            Intent intent = new Intent(context, UpdateReceiver.class);
            long clock_real;
            //Time to execute next petition
            if (actual_time != -1) {
                long now = SystemClock.elapsedRealtime();
                clock_real = actual_time;
                do {
                    clock_real += 60000;
                } while (clock_real < now);
            } else {
                long time = new Date().getTime();
                //sync alarm to start of minute
                clock_real = SystemClock.elapsedRealtime() + 60000 - (time - (time/60000)*60000);

            }
            int clock_offset = (int) (Math.random() * 5000); // 5 segons de marge
            long clock_use = clock_real+clock_offset;
            //Data principal per obtenir dades
            if (previous_date != -1) {
                intent.putExtra(EXTRA_PREVIOUSTIME, previous_date);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("prev_date", previous_date);
                editor.apply();
            } else {
                long p = sharedPreferences.getLong("prev_date", -1);
                if (p != -1) {
                    intent.putExtra(EXTRA_PREVIOUSTIME, p);
                }
            }

            intent.putExtra(EXTRA_REALTIME, clock_real);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            //Call intent
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, clock_use, pendingIntent);
            } else {
                alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, clock_use, pendingIntent);
            }
        } else {
            //Not logged in
            App.logIn(context);
        }
    }
}
