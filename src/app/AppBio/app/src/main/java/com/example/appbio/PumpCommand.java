package com.example.appbio;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

public class PumpCommand {

    private final ServerRequest serverRequest;
    private final JSONObject jsonRequest;

    public PumpCommand(int id, Object command) {
        jsonRequest = new JSONObject();
        serverRequest = new ServerRequest(App.serverAddress + "data/" + id, "PUT");
        setCommand(command);
    }

    public void setCommand(Object command) {
        try {
            jsonRequest.put("command", command);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendCommand(Context context) {
        serverRequest.setMessage(jsonRequest.toString());
        serverRequest.setContentType("application/json");

        if (context != null) Toast.makeText(context,R.string.toast_sending,Toast.LENGTH_SHORT).show();
        ServerRequest.requestExecutor.execute(() -> {
            serverRequest.sendRequest();
            if (context != null) {
                ContextCompat.getMainExecutor(context).execute(() -> {
                    if (serverRequest.getCode() == 204) {
                        Toast.makeText(context, R.string.toast_sent, Toast.LENGTH_LONG).show();
                    } else {
                        Log.d("code", String.valueOf(serverRequest.getCode()));
                        new AlertErrorBuilder(context, R.string.error_command).show();
                    }
                });
            }
        });
    }
}
