package com.example.appbio;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

public class XMLComponentParser {

    public static String getName(InputStream in, String name) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(in, null);
        parser.nextTag();
        return findValue(parser, name, "name");
    }

    public static String getDescription(InputStream in, String name) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(in, null);
        parser.nextTag();
        return findValue(parser, name, "description");
    }

    public static String getUnit(InputStream in, String name) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(in, null);
        parser.nextTag();
        return findValue(parser, name, "unit");
    }

    private static String findValue(XmlPullParser parser, String name, String value) throws IOException, XmlPullParserException {
        findName(parser, name);
        int event = parser.getEventType();
        while (!(event == XmlPullParser.END_TAG && parser.getName().equals(value))) {
            if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(value)) {
                parser.next();
                String text = parser.getText();
                if (text == null) {
                    return "";
                }
                return text;
            }
            event = parser.next();
        }
        return null;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private static void findName(XmlPullParser parser, String name) throws IOException, XmlPullParserException {
        while(!( parser.next() == XmlPullParser.START_TAG &&
                parser.getName().equals("string") &&
                parser.getAttributeValue(null, "name").equals(name))
        && !(parser.getEventType() == XmlPullParser.END_TAG && parser.getName().equals("resources")));
    }
}
