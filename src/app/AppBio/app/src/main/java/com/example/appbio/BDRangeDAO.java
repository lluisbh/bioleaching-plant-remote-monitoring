package com.example.appbio;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface BDRangeDAO {
    @Insert
    void insert(BDRangeEntity rangeEntity);

    @Update
    void update(BDRangeEntity rangeEntity);

    @Delete
    void delete(BDRangeEntity rangeEntity);

    @Query("DELETE FROM Range")
    void deleteAll();

    @Query("DELETE FROM Range WHERE element = :element")
    void deleteAllElement(int element);

    @Query("DELETE FROM Range WHERE element = :element AND dateFrom > :from AND dateTo < :to")
    void deleteRange(int element, long from, long to);

    @Query("SELECT * FROM Range WHERE element = :element AND dateFrom <= :dateFrom AND dateTo >= :dateTo LIMIT 1")
    BDRangeEntity getRangeWith(int element, long dateFrom, long dateTo);

    @Query("SELECT * FROM Range WHERE element = :element AND dateTo+1 >= :dateFrom AND dateFrom < :dateFrom ORDER BY dateTo ASC LIMIT 1")
    BDRangeEntity getRangeWithUp(int element, long dateFrom);

    @Query("SELECT * FROM Range WHERE element = :element AND dateFrom-1 <= :dateTo AND dateTo > :dateTo ORDER BY dateFrom DESC LIMIT 1")
    BDRangeEntity getRangeWithDown(int element, long dateTo);
}
