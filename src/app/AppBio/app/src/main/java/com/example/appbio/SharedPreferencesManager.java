package com.example.appbio;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPreferencesManager {

    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SharedPreferencesManager(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    @SuppressLint("CommitPrefEdits")
    public SharedPreferencesManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        editor = this.sharedPreferences.edit();
    }

    public void setIsAdmin(boolean value) { editor.putBoolean("admin", value); }

    public void setIsLoggedIn(boolean value) {
        editor.putBoolean("logged_in", value);
    }

    public boolean isLoggedIn() { return sharedPreferences.getBoolean("logged_in", false); }

    public boolean isAdmin() { return sharedPreferences.getBoolean("admin", false);}

    public void apply() {
        editor.apply();
    }
}
