package com.example.appbio;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class CSVFile {

    private final int columns;
    private final ArrayList<String[]> elements = new ArrayList<>();

    public CSVFile(int columns) {
        this.columns = columns;
    }

    public void addRow(String... args) {
        String[] row = new String[columns];

        int len = args.length;
        System.arraycopy(args, 0, row, 0, len);
        elements.add(row);
    }

    @NonNull
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String[] row:
             elements) {
            for (String i:
                row) {
                result.append(i);
                result.append(',');
            }
            result.setCharAt(result.length()-1, '\n');
        }
        return result.toString();
    }
}
