package com.example.appbio;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class NotificationAlert {

    private static NotificationAlert INSTANCE;

    private final NotificationCompat.Builder builder;
    private int notificationId = 0;

    private static final String CHANNEL_ID = "com.example.bioapp.ALERT_CHANNEL";

    private static void createNotificationChannel(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationAlert = context.getSystemService(NotificationManager.class);
            notificationAlert.createNotificationChannel(channel);
        }
    }

    private NotificationAlert(Context context) {
        builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_bioapp)//Icona de les notificacions
                .setContentTitle(context.getString(R.string.notification_title))//títol
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true);


    }
    public void SetAlert(Context context, CharSequence textContent, int stationId) {
        Intent intentMenu = new Intent(context, ActivityMainMenu.class);
        Intent intentAlerts = new Intent(context, ActivityAlerts.class);
        Intent intentStation = new Intent(context, ActivityStation.class);
        //intentStation.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentStation.putExtra(ActivityStation.EXTRA_STATION, stationId);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intentMenu);
        stackBuilder.addNextIntent(intentAlerts);
        stackBuilder.addNextIntent(intentStation);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder build = builder.setContentIntent(pendingIntent).setContentText(textContent)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(textContent));
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId++, build.build());
    }

    public static NotificationAlert getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (NotificationAlert.class) {
                if (INSTANCE == null) {
                    INSTANCE = new NotificationAlert(context);
                    createNotificationChannel(context);
                }
            }
        }
        return INSTANCE;
    }
}
