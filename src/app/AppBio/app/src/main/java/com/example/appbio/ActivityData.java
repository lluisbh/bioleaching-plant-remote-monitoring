package com.example.appbio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ActivityData extends ActivityBase {

    int filterWindowSize;
    private final int ROWS_PAGE = 50;

    private final ExecutorService databaseWriteExecutor =
            Executors.newSingleThreadExecutor();

    private BDRoom bdRoom;
    private BDRegisterEntity[] registerEntities = new BDRegisterEntity[0];
    private String[] entityNames;
    HashMap<Integer, String> mapNames;
    private int[] entityIds;
    private Spinner spinnerPage;
    private Spinner spinnerElement;

    RecyclerView recyclerView;

    private long filterDateStart = 0;
    private long filterDateEnd = -1;

    private boolean booleanResults = false;

    public static final String EXTRA_ELEMENT = "com.example.appbio.ELEMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_data);
        super.onCreate(savedInstanceState);

        //Mida de la finestra de filtres
        View scrollFiltres = findViewById(R.id.scrollFiltres);
        ConstraintLayout root = findViewById(R.id.layoutData);
        ViewTreeObserver vto = root.getViewTreeObserver();
        //Inicialitza la altura de la finestra
        vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                root.getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                filterWindowSize = scrollFiltres.getHeight();

                View layout = findViewById(R.id.layoutFiltres);
                layout.setVisibility(View.GONE);
            }
        });

        //Setup de l'spinner de la pàgina
        spinnerPage = findViewById(R.id.spinnerPagina);
        spinnerPage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setupPage(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Temps d'inici i fi, per defecte 24 hores
        long now = new Date().getTime();
        ((Button) findViewById(R.id.buttonDateEnd)).setText(DateFormat.format
                ("dd/MM/yyyy HH:mm", now).toString());
        filterDateEnd = now/60000;

        now -= 24*60*60000;
        ((Button) findViewById(R.id.buttonDateStart)).setText(DateFormat.format
                ("dd/MM/yyyy HH:mm", now).toString());
        filterDateStart = now/60000;

        //Setup elements i tot relacionat
        spinnerElement = findViewById(R.id.spinnerComponent);

        bdRoom = BDRoom.getDatabase(this);

        Context context = this;

        BDElementDAO bdElementDAO = bdRoom.elementDAO();
        BDRoom.databaseWriteExecutor.execute(() -> {

            List<BDElementEntity> entities = bdElementDAO.getAllElementsNonLive();

            int len = entities.size();
            //Entity names and filter by id for reference
            entityNames = new String[len + 1];
            mapNames = new HashMap<>();
            entityNames[0] = getString(R.string.data_all);
            //Guarda els ids
            entityIds = new int[len];
            for (int i = 0; i < len; i++) {
                BDElementEntity entity = entities.get(i);
                entityNames[i + 1] = entity.getNameFinal(context);
                entityIds[i] = entity.getId();
                mapNames.put(entity.getId(), entity.getNameFinal(context));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, entityNames);
            runOnUiThread(() -> {
                spinnerElement.setAdapter(adapter);

                getExtras(getIntent());

                //Llista de registres
                recyclerView = findViewById(R.id.recyclerRegisters);
                AdapterData adapter1 = new AdapterData(new BDRegisterEntity[0], mapNames, booleanResults);
                recyclerView.setAdapter(adapter1);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                recyclerView.scrollToPosition(0);
            });
        });
    }

    @Override
    protected void onNewIntent (Intent intent) {
        super.onNewIntent(intent);
        getExtras(intent);
    }

    public void onClickDate(View v){
        Button b = (Button) v;

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        Context context = this;

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                            (view1, hourOfDay, minute) -> {
                                // Get Current Time
                                Calendar c1 = Calendar.getInstance();
                                c1.set(year, monthOfYear,dayOfMonth, hourOfDay, minute);
                                if (v == findViewById(R.id.buttonDateStart)) {
                                    filterDateStart = c1.getTimeInMillis()/60000;
                                } else {
                                    filterDateEnd = c1.getTimeInMillis()/60000;
                                }
                                b.setText(DateFormat.format("dd/MM/yyyy HH:mm", c1.getTime()).toString());
                            }, mHour, mMinute, true);
                    timePickerDialog.show();
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public void onClickFiltres(@SuppressWarnings("unused") View v){
        View layout = findViewById(R.id.layoutFiltres);
        View scrollFiltres = findViewById(R.id.scrollFiltres);
        View helper = findViewById(R.id.helperFiltres);

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) scrollFiltres.getLayoutParams();

        if (layout.getVisibility() == View.VISIBLE) {
            //Amagar pantalla
            params.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;

            layout.setVisibility(View.GONE);

        } else {
            //Mostrar pantalla

            int helperHeight = helper.getHeight();
            if (filterWindowSize < helperHeight) {
                params.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
            } else {
                params.height = helperHeight;
            }

            layout.setVisibility(View.VISIBLE);
        }
    }

    public void onClickBuscar(@SuppressWarnings("unused") View v) {
        //Obtenir valors del filtre
        int position = ((Spinner) findViewById(R.id.spinnerComponent)).getSelectedItemPosition() - 1;
        int component;
        if (position == -1) component = -1;
        else component = entityIds[position];
        long now = new Date().getTime()/60000;
        long dateFrom = Math.min(filterDateStart, now);
        long dateTo;
        if (filterDateEnd == -1) dateTo = now;
        else dateTo = Math.min(filterDateEnd, now);

        if (dateFrom > dateTo) {
            AlertErrorBuilder alertErrorBuilder = new AlertErrorBuilder(this, R.string.error_filter);
            alertErrorBuilder.show();
            return;
        }

        //get data from server
        DataRequest.RunnableGetData dataRunnable = DataRequest.getData(dateFrom, dateTo, component, this);

        Context activity = this;
        //wait (on another thread)
        databaseWriteExecutor.execute(() -> {
            synchronized (dataRunnable) {
                if (dataRunnable.isRunning()) {
                    try {
                        dataRunnable.wait(7000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!dataRunnable.isSuccessful()) {
                    runOnUiThread( () -> {
                        AlertErrorBuilder alertErrorBuilder = new AlertErrorBuilder(activity,
                                R.string.error_command);
                        alertErrorBuilder.show();
                    });
                    return;
                }
            }

            BDRoom.databaseWriteExecutor.execute(() -> {
                //get data
                BDRegisterDAO dao = bdRoom.registerDAO();
                if (component == -1) {
                    registerEntities = dao.getAllRegistersFiltered(dateFrom, dateTo);
                } else {
                    registerEntities = dao.getElementRegistersFilteredNonLive(component, dateFrom, dateTo);

                    BDElementDAO elementDAO = bdRoom.elementDAO();
                    booleanResults = elementDAO.getElementByIdNonLive(component).getValueType()
                            == BDElementDAO.TYPE_BOOLEAN;
                }

                //Si existeixen dates abans del especificat, posar aquesta
                int len = registerEntities.length;
                for (int i = 0; i < len; i++) {
                    BDRegisterEntity reg = registerEntities[i];
                    long d = reg.getDate();
                    if (d < dateFrom) {
                        registerEntities[i] = new BDRegisterEntity(reg.getElement(), reg.getNumber(), dateFrom);
                    }
                }

                runOnUiThread(() -> {
                    // Update page spinner
                    int numPages = (int) Math.ceil((double)registerEntities.length/ROWS_PAGE);
                    String[] arrayNumbers;
                    if (numPages > 0) {
                        arrayNumbers = new String[numPages];
                        for (int i = 0 ; i < numPages ; i++) arrayNumbers[i] = String.valueOf(i+1);
                    } else {
                        arrayNumbers = new String[1];
                        arrayNumbers[0] = "1";
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, arrayNumbers);
                    spinnerPage.setAdapter(adapter);

                    spinnerPage.setClickable(true);
                    spinnerPage.setVisibility(View.VISIBLE);
                });
            });
        });
    }

    public void onClickGuardar(@SuppressWarnings("unused") View v) {
        //guardar les dades mostrades dins d'un fitxer
        CSVFile file = new CSVFile(3);
        file.addRow(getString(R.string.text_component), getString(R.string.text_date), getString(R.string.text_value));
        for (BDRegisterEntity reg:
            registerEntities) {
            file.addRow(entityNames[reg.getElement()+1],
                    DateFormat.format("dd/MM/yyyy HH:mm", reg.getDate()*60000).toString(),
                    String.valueOf(reg.getNumber())
            );
        }

        String send = file.toString();

        try {
            File outputDir = new File(this.getCacheDir(), "export"); // context being the Activity pointer
            boolean exists = outputDir.exists();
            if (!exists) {
                exists = outputDir.mkdir();
            }
            if (exists) {
                //Crear fitxer temporal per donar-li nom
                File outputFile = File.createTempFile("Registre", ".csv", outputDir);
                FileWriter fileWriter = new FileWriter(outputFile);
                fileWriter.write(send);
                fileWriter.close();

                //Començar activitat d'enviar
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                Uri parsed = FileProvider.getUriForFile(this,
                        "com.example.appbio.fileprovider", outputFile);

                sendIntent.putExtra(Intent.EXTRA_STREAM, parsed);
                sendIntent.setType("text/csv");

                Intent shareIntent = Intent.createChooser(sendIntent, null);

                List<ResolveInfo> resInfoList = this.getPackageManager().queryIntentActivities(shareIntent, PackageManager.MATCH_DEFAULT_ONLY);

                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    this.grantUriPermission(packageName, parsed, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }

                startActivity(shareIntent);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupPage(int page) {
        //Ensenya registres a cada pàgina
        BDRegisterEntity[] pageRegisters = Arrays.copyOfRange(registerEntities,
                ROWS_PAGE*page, Math.min(ROWS_PAGE*(page+1), registerEntities.length));
        //put data
        AdapterData adapter = new AdapterData(pageRegisters, mapNames, booleanResults);
        recyclerView.setAdapter(adapter);
    }

    private void getExtras(Intent intent) {
        int element = intent.getIntExtra(EXTRA_ELEMENT, -1);
        Spinner spinner = findViewById(R.id.spinnerComponent);

        if (element != -1) {
            int len = entityIds.length;
            for (int i = 0; i < len; i++) {
                if (entityIds[i] == element) {
                    spinner.setSelection(i+1);
                    break;
                }
            }

            onClickBuscar(null);
        } else {
            spinner.setSelection(-1);
        }
    }
}