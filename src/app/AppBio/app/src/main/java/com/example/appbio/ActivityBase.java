package com.example.appbio;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

//Class for all activities inside the application itself.
//"Inside the application" refers to being logged in.

public class ActivityBase extends AppCompatActivity {

    private int activityLayout;

    private DrawerLayout drawerLayout;

    protected void setActivityLayout(int id) {
        activityLayout = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_inapp);

        setUpToolbar();

        ViewGroup navController = findViewById(R.id.home_content);
        getLayoutInflater().inflate(activityLayout, navController);

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);
        if (!sharedPreferencesManager.isLoggedIn()) {
            App.logIn(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);
        //reanudar quan s'ha suspés l'app
        setupDataSchedule(sharedPreferencesManager);
    }

    private void setupDataSchedule(SharedPreferencesManager sharedPreferencesManager) {
        //iniciar schedule
        if (sharedPreferencesManager.isLoggedIn()) {
            DataUpdateSchedule.startSchedule(this);
        }
    }

    private void setUpToolbar() {
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, myToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);
        ((NavigationView) findViewById(R.id.navigation_view)).getMenu().getItem(2)
                .setVisible(sharedPreferencesManager.isAdmin());
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void onClickStation(MenuItem menuItem) {
        int station;
        int menuId = menuItem.getItemId();
        if (menuId == R.id.nav_bio) {
            station = 0;
        } else if (menuId == R.id.nav_tank_1) {
            station = 1;
        } else if (menuId == R.id.nav_lix) {
            station = 2;
        } else if (menuId == R.id.nav_tank_2) {
            station = 4;
        } else if (menuId == R.id.nav_recup) {
            station = 3;
        } else {
            station = 0;
        }
        Intent intent = new Intent(this, ActivityStation.class);
        intent.putExtra(ActivityStation.EXTRA_STATION, station);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);
        stackBuilder.startActivities();
    }

    public void onClickAlerts(@SuppressWarnings("unused") MenuItem menuItem) {
        Intent intent = new Intent(this, ActivityAlerts.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        drawerLayout.closeDrawer(GravityCompat.START);
        startActivity(intent);
    }

    public void onClickUsers(@SuppressWarnings("unused") MenuItem menuItem) {
        Intent intent = new Intent(this, ActivityUsers.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        drawerLayout.closeDrawer(GravityCompat.START);
        startActivity(intent);
    }

    public void onClickData(@SuppressWarnings("unused") MenuItem menuItem) {
        Intent intent = new Intent(this, ActivityData.class);
        drawerLayout.closeDrawer(GravityCompat.START);
        startActivity(intent);
    }

    public void onClickLogout(@SuppressWarnings("unused") MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);
        App.logOut(this);
    }
}
