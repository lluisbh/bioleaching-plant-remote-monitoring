package com.example.appbio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ActivityUsers extends ActivityBase {

    private static final ExecutorService executor = Executors.newSingleThreadExecutor();

    private String[] usersId;
    private int userChoosed;

    private AlertDialog alertDialogAdd;
    private AlertDialog alertDialogEdit;
    private AlertDialog alertDialogDelete;

    private AlertErrorBuilder alertErrorCommand;
    private AlertErrorBuilder alertErrorData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_users);
        super.onCreate(savedInstanceState);

        alertErrorData = new AlertErrorBuilder(this, R.string.error_data);
        alertErrorCommand = new AlertErrorBuilder(this, R.string.error_command);

        updatePage();

        setupAddAlert();
        setupEditAlert();
        setupDeleteAlert();

    }

    @Override
    protected void onNewIntent (Intent intent) {
        super.onNewIntent(intent);
        updatePage();
    }

    private void updatePage() {
        LinearLayout linearLayout = findViewById(R.id.LinearLayoutUsers);
        linearLayout.removeAllViews();
        executor.execute(() -> {
            DataRequest.RunnableGetUsers run = DataRequest.getUsers();
            boolean success;
            synchronized (run) {
                if (run.isRunning()) {
                    try {
                        run.wait(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                success = run.isSuccessful();
                if (success) {
                    runOnUiThread(() -> {
                        LayoutInflater layoutInflater = getLayoutInflater();
                        usersId = run.getNames();
                        for (String s:
                                usersId) {
                            ConstraintLayout rowView = (ConstraintLayout) layoutInflater.
                                    inflate(R.layout.users_row, linearLayout, false);
                            ((TextView) rowView.getChildAt(0)).setText(s);
                            linearLayout.addView(rowView);
                        }
                    });
                } else {
                    runOnUiThread(() -> alertErrorData.show());
                }
            }
        });
    }

    private void setupAddAlert() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setTitle(R.string.text_add);
        alertBuilder.setPositiveButton(R.string.input_accept, null);
        AlertDialog alert = alertBuilder.create();

        LayoutInflater layoutInflater = getLayoutInflater();
        AlertDialog.Builder alertDialogAddBuild = new AlertDialog.Builder(this);
        alertDialogAddBuild.setTitle(R.string.text_add);
        View addView = layoutInflater.inflate(R.layout.user_alert_add,null);
        alertDialogAddBuild.setView(addView);

        alertDialogAddBuild.setNegativeButton(R.string.input_cancel, (dialog, which) -> {
            ConstraintLayout constraintLayout = (ConstraintLayout) addView;
            ((TextView) constraintLayout.getChildAt(0)).setText("");
            ((TextView) constraintLayout.getChildAt(1)).setText("");
            ((SwitchCompat) constraintLayout.getChildAt(2)).setChecked(false);
        });
        alertDialogAddBuild.setPositiveButton(R.string.input_send, (dialog, which) -> {
            ConstraintLayout constraintLayout = (ConstraintLayout) addView;
            TextView textName = (TextView) constraintLayout.getChildAt(0);
            TextView textPassword = (TextView) constraintLayout.getChildAt(1);
            SwitchCompat switchAdmin = (SwitchCompat) constraintLayout.getChildAt(2);

            String name = textName.getText().toString();
            String password = textPassword.getText().toString();
            boolean admin = switchAdmin.isChecked();

            ServerRequest.requestExecutor.execute(() -> {
                UserCommand.UserCharacteristics characteristics = UserCommand.userAdd(name, password, admin);
                runOnUiThread(() -> {
                    if (characteristics != null) {
                        alert.setMessage(getString(R.string.text_user_info,
                                characteristics.username, characteristics.password, characteristics.admin));
                        alert.show();
                    } else {
                        alertErrorCommand.show();
                    }
                    updatePage();
                });

            });

            textName.setText("");
            textPassword.setText("");
            switchAdmin.setChecked(false);

        });
        alertDialogAdd = alertDialogAddBuild.create();
    }

    private void setupEditAlert() {
        LayoutInflater layoutInflater = getLayoutInflater();
        Context context = this;

        AlertDialog.Builder alertDialogEditBuild = new AlertDialog.Builder(this);
        alertDialogEditBuild.setTitle(R.string.text_edit);
        View editView = layoutInflater.inflate(R.layout.user_alert_edit,null);
        alertDialogEditBuild.setView(editView);
        alertDialogEditBuild.setNegativeButton(R.string.input_cancel, (dialog, which) ->
                ((TextView) ((ConstraintLayout) editView).getChildAt(0)).setText(""));
        alertDialogEditBuild.setPositiveButton(R.string.input_send, (dialog, which) -> {
            ConstraintLayout constraintLayout = (ConstraintLayout) editView;
            TextView textPassword = (TextView) constraintLayout.getChildAt(0);

            String password = textPassword.getText().toString();

            ServerRequest.requestExecutor.execute(() -> {
                boolean success = UserCommand.userChange(usersId[userChoosed], password);
                runOnUiThread(() -> {
                    if (success) {
                        Toast.makeText(context, R.string.toast_user_updated, Toast.LENGTH_SHORT).show();
                    } else {
                        alertErrorCommand.show();
                    }
                });

            });

            textPassword.setText("");
        });
        alertDialogEdit = alertDialogEditBuild.create();
    }

    private void setupDeleteAlert() {
        AlertDialog.Builder alertDialogDeleteBuild = new AlertDialog.Builder(this);
        Context context = this;

        alertDialogDeleteBuild.setTitle(R.string.text_delete);
        alertDialogDeleteBuild.setMessage(R.string.text_delete_confirm);
        alertDialogDeleteBuild.setNegativeButton(R.string.input_cancel,null);
        alertDialogDeleteBuild.setPositiveButton(R.string.input_accept, (dialog, which) ->
                ServerRequest.requestExecutor.execute(() -> {
                    boolean success = UserCommand.userRemove(usersId[userChoosed]);
                    runOnUiThread(() -> {
                        if (success) {
                            Toast.makeText(context, R.string.toast_user_deleted, Toast.LENGTH_SHORT).show();
                        } else {
                            alertErrorCommand.show();
                        }
                        updatePage();
                    });

        }));
        alertDialogDelete = alertDialogDeleteBuild.create();
    }

    public void onClickAdd(@SuppressWarnings("unused") View v) {

        alertDialogAdd.show();
    }

    public void onClickEdit(View view) {
        View parent = (View) view.getParent();
        userChoosed = ((ViewGroup) parent.getParent()).indexOfChild(parent);
        alertDialogEdit.show();
    }

    public void onClickRemove(View view) {
        View parent = (View) view.getParent();
        userChoosed = ((ViewGroup) parent.getParent()).indexOfChild(parent);
        alertDialogDelete.show();
    }
}