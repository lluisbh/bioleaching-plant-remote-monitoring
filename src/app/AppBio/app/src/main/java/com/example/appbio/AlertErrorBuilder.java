package com.example.appbio;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

public class AlertErrorBuilder {
    //Classe per crear finestres emergents

    private final AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;

    public AlertErrorBuilder(Context context, int descriptionId) {
        alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(R.string.error_error);
        alertDialogBuilder.setMessage(descriptionId);
        alertDialogBuilder.setPositiveButton(R.string.input_accept,null);
    }

    public void show() {
        if (alertDialog == null) {
            alertDialog = alertDialogBuilder.create();
        }
        alertDialog.show();
    }
}
