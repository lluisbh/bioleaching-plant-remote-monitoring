package com.example.appbio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LiveData;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivityElement extends ActivityBase {

    private LineChartManager chartManager;
    private AlertDialog alertDialogAct;
    private View viewAlert;
    private boolean isAdmin = false;

    private int elementId = 0;
    private String unit;
    private boolean canAct = false;

    private static final int TYPE_REAL = 0;
    private static final int TYPE_INTEGER = 1;
    private static final int TYPE_BOOLEAN = 2;
    private int typeGraph;

    private float value;
    private float maxValue = -1;
    private float minValue = -1;

    private PumpCommand pumpCommand;

    private BDRoom bdRoom;

    public static final String EXTRA_ELEMENT = "com.example.appbio.ELEMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_element);
        super.onCreate(savedInstanceState);

        elementId = getIntent().getIntExtra(EXTRA_ELEMENT, 0);
        bdRoom = BDRoom.getDatabase(this);
        //Gràfica
        chartManager = new LineChartManager(findViewById(R.id.lineChart));
        chartManager.setXRange(0, 1439);

        //És administrador?
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);
        isAdmin = sharedPreferencesManager.isAdmin();

        setupNameDescription();
    }

    private void setupNameDescription() {
        Context context = this;
        BDRoom.databaseWriteExecutor.execute(() -> {
            BDElementDAO bdElementDAO = bdRoom.elementDAO();
            BDElementEntity element = bdElementDAO.getElementByIdNonLive(elementId);
            int valueType = element.getValueType();

            canAct = element.canAct() && isAdmin;
            maxValue = element.getMaxVal();
            minValue = element.getMinVal();

            runOnUiThread(() -> {

                switch (valueType) {
                    case BDElementDAO.TYPE_REAL: {
                        //És un valor decimal
                        float top = element.getMaxVal();
                        if (top != -1) {
                            chartManager.setLine(top);
                        }
                        float bottom = element.getMinVal();
                        if (bottom != -1) {
                            chartManager.setLine(bottom);
                        }
                        typeGraph = TYPE_REAL;
                        unit = element.getUnitFinal(context);

                        /*if (canAct) {
                            //sense implementar
                            createAlert();
                            findViewById(R.id.buttonCommand).setVisibility(View.VISIBLE);
                        }*/

                        break;
                    }
                    case BDElementDAO.TYPE_INTEGER: {
                        //És un valor decimal
                        int top = (int) element.getMaxVal();
                        if (top != -1) {
                            chartManager.setLine(top);
                        }
                        int bottom = (int) element.getMinVal();
                        if (bottom != -1) {
                            chartManager.setLine(bottom);
                        }
                        typeGraph = TYPE_INTEGER;
                        unit = element.getUnitFinal(context);

                        if (canAct) {
                            createAlertInt();
                            findViewById(R.id.buttonCommand).setVisibility(View.VISIBLE);
                        }

                        break;
                    }
                    //case BDElementDAO.TYPE_BOOLEAN:
                    default: {
                        //És un valor decimal
                        typeGraph = TYPE_BOOLEAN;
                        String[] array = {getString(R.string.data_off), getString(R.string.data_on)};
                        chartManager.setBooleanAxis(array);

                        if (canAct) {
                            createAlertBoolean();
                            findViewById(R.id.buttonCommand).setVisibility(View.VISIBLE);
                        }

                        break;
                    }
                }

                //Títol/descripció
                TextView textTitle = findViewById(R.id.title_element);
                textTitle.setText(element.getNameFinal(context));
                TextView textDesc = findViewById(R.id.textDescription);
                textDesc.setText(element.getDescription(context));

                setupData();
            });
        });
    }

    private void setupData() {
        DataRequest.getLast24hours(this, elementId);
        BDRegisterDAO bdRegisterDAO = bdRoom.registerDAO();
        LiveData<List<BDRegisterEntity>> dades = bdRegisterDAO.getElementRegistersFiltered(elementId,
                (int) (new Date().getTime() / 60000) - 24 * 60, Long.MAX_VALUE);

        switch (typeGraph) {
            case TYPE_BOOLEAN:
                pumpCommand = new PumpCommand(elementId, false);
            case TYPE_INTEGER:
                pumpCommand = new PumpCommand(elementId, 0);
        }

        dades.observe(this, o -> {
            TextView textLast = findViewById(R.id.textLastValue);
            int size = o.size();
            if (size == 0) {
                // Sense dades
                textLast.setText(R.string.error_nodata);
            } else {
                //Mostrar valor actual
                BDRegisterEntity lastReg = o.get(size-1);
                value = lastReg.getNumber();
                if (unit != null) {
                    //Valor i unitat
                    if (typeGraph == TYPE_INTEGER) {
                        textLast.setText(getString(R.string.data_value_int_unit, (int) value, unit));
                    } else if (typeGraph == TYPE_REAL) {
                        textLast.setText(getString(R.string.data_value_real_unit, value, unit));
                    }
                } else if (typeGraph == TYPE_BOOLEAN){
                    //Valor com ON/OFF

                    if (value == 0)
                        textLast.setText(R.string.data_off);
                    else
                        textLast.setText(R.string.data_on);

                    if (canAct && alertDialogAct.isShowing()) {
                        switch (typeGraph) {
                            case TYPE_BOOLEAN:
                                boolean bombaEncesa = value != 0;
                                pumpCommand.setCommand(bombaEncesa);
                                modifyAlertBoolean();
                                break;
                            case TYPE_INTEGER:
                                pumpCommand.setCommand((int) value);
                                //modifyAlertInt();
                                break;
                            default:
                                break;
                            }
                    }

                } else {
                    //Valor sol
                    if (typeGraph == TYPE_INTEGER) {
                        textLast.setText(getString(R.string.data_value_int_unit, lastReg.getNumber(), ""));
                    } else if (typeGraph == TYPE_REAL) {
                        textLast.setText(getString(R.string.data_value_real_unit, lastReg.getNumber(), ""));
                    }
                }


                // Actualitzar gràfica
                // Primer temps de la taula ha de ser al valor 0
                // Comença 24 hores abans
                long now = (int) (new Date().getTime()/60000) - 24*60;

                ArrayList<Entry> chartList = new ArrayList<>();

                // Buscar primer registre en rang de 24 hores
                // binary search
                int start = 0;
                int end = size;
                int mid = (end-start)/2;
                long v = o.get(mid).getDate() - now;
                while (mid > 0 && mid < size-1 &&
                        start < end && !(v >= 0 && o.get(mid-1).getDate() - now < 0)) {

                    if (v > 0) {
                        end = mid;
                    } else {
                        start = mid;
                    }
                    mid = start+(end-start)/2;
                    v = o.get(mid).getDate() - now;
                }

                BDRegisterEntity firstReg = o.get(mid);
                long firstDate = firstReg.getDate()-now;
                if (firstDate != 0) {
                    if (firstDate < 0){
                        //Si el primer registre es troba abans de les últimes 24 hores, posar-lo com a valor inicial a la gràfica
                        firstReg = o.get(mid);
                        chartList.add(new Entry(0, firstReg.getNumber()));
                    } else if (mid > 0){
                        //Si hi ha un valor inicial previ, posar-lo
                        firstReg = o.get(mid-1);
                        chartList.add(new Entry(0, firstReg.getNumber()));
                    }
                }

                if (firstDate >= 0) {
                    for (int i = mid; i < size; i++) {
                        //Afegir els valors a la gràfica
                        BDRegisterEntity entry = o.get(i);
                        long date_real = entry.getDate();
                        long date = date_real - now;
                        //Gràfica quadrada per valors binaris
                        if (typeGraph==TYPE_BOOLEAN && i > 0) chartList.add(new Entry(date-1, o.get(i-1).getNumber()));

                        chartList.add(new Entry(date, entry.getNumber()));
                    }
                }

                //Si no hi ha un registre al final de tot, afegir-lo a la gràfica
                if (lastReg.getDate()-now != 1439) {
                    chartList.add(new Entry(1439, lastReg.getNumber()));
                }

                //Posar dades a la gràfica
                chartManager.setData(chartList);

                // Setup dates a l'eix horitzontal
                String[] stringArray = new String[1440];
                long date_current = now*60000;
                for (int i = 0; i < 1440; i++) {
                    stringArray[i] = DateFormat.format("HH:mm", date_current).toString();
                    date_current += 60000;
                }

                chartManager.setAxis(stringArray);
            }
        });
    }

    private void createAlert(View subView) {
        viewAlert = subView;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.text_command);
        alertDialog.setView(subView);

        Activity activity = this;
        alertDialog.setPositiveButton(R.string.input_send, (dialog, which) -> {
            switch (typeGraph) {
                case TYPE_BOOLEAN:
                    pumpCommand.setCommand(((ToggleButton) viewAlert).isChecked());
                    pumpCommand.sendCommand(activity);
                    break;
                case TYPE_INTEGER:
                    pumpCommand.setCommand(Integer.parseInt(((EditText) viewAlert).getText().toString()));
                    pumpCommand.sendCommand(activity);
                    break;
                default:
                    break;
            }
        });
        alertDialog.setNegativeButton(R.string.input_cancel,null);
        alertDialogAct = alertDialog.create();
    }

    private void createAlertInt() {
        EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (!text.isEmpty()) {
                    int value = Integer.parseInt(s.toString());
                    if (value < minValue) {
                        editText.setText(String.valueOf((int) minValue));
                    } else if (value > maxValue) {
                        editText.setText(String.valueOf((int) maxValue));

                    }
                    //modifyAlertInt();
                } else {
                    alertDialogAct.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            }
        });
        createAlert(editText);
    }

    /*private void modifyAlertInt() {
        //Actualitzar l'alerta amb noves dades
        String text = ((EditText) viewAlert).getText().toString();
        alertDialogAct.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(!text.isEmpty() && ((int) value) !=
                Integer.parseInt(text));
    }*/

    private void createAlertBoolean() {
        //Alerta de control
        //sense ús
        ToggleButton toggleButton = new ToggleButton(this);
        toggleButton.setTextOff(getString(R.string.text_pump_off));
        toggleButton.setTextOn(getString(R.string.text_pump_on));
        toggleButton.setOnCheckedChangeListener(
                (buttonView, isChecked) -> alertDialogAct.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(isChecked != (value != 0)));

        createAlert(toggleButton);
    }

    private void modifyAlertBoolean() {
        //Actualitzar l'alerta amb noves dades
        //alertDialogAct.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled((value != 0) != ((ToggleButton) viewAlert).isChecked());
        String state;
        if (value != 0)
            state = getString(R.string.text_pump_on);
        else
            state = getString(R.string.text_pump_off);
        alertDialogAct.setMessage(getString(R.string.data_alert_pump, state));
    }

    public void onClickComandament(@SuppressWarnings("unused") View v) {
        //Mostrar comandament d'aquesta bomba
        switch (typeGraph) {
            case TYPE_BOOLEAN:
                alertDialogAct.show();
                ((ToggleButton) viewAlert).setChecked(value != 0);
                modifyAlertBoolean();
                break;
            case TYPE_INTEGER:
                alertDialogAct.show();
                ((EditText) viewAlert).setText(String.valueOf((int) value));
                //modifyAlertInt();
            default:
                break;
        }

    }

    public void onClickHistorial(@SuppressWarnings("unused") View v) {
        //Obrir finestra de dades amb les últimes 24 hores d'aquest element
        Intent intent = new Intent(this, ActivityData.class);

        intent.putExtra(ActivityData.EXTRA_ELEMENT, elementId);

        startActivity(intent);
    }
}