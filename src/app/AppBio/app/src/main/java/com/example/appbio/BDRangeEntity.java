package com.example.appbio;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "Range",
        foreignKeys = {
        @ForeignKey(entity = BDElementEntity.class,
                parentColumns = {"id"},
                childColumns = {"element"})},
        indices = {@Index(value = {"element"})}
        )
public class BDRangeEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private final int element;

    private long dateFrom;

    private long dateTo;

    public BDRangeEntity(int element, long dateFrom, long dateTo) {
        this.element = element;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public void setId(int id) { this.id = id; }

    public int getId() { return id; }

    public int getElement() { return element; }

    public long getDateFrom() { return dateFrom; }

    public void setDateFrom(long dateFrom) { this.dateFrom = dateFrom; }

    public long getDateTo() { return dateTo; }

    public void setDateTo(long dateTo) { this.dateTo = dateTo; }
}
