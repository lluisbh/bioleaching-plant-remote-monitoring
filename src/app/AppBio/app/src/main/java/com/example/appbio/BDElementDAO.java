package com.example.appbio;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BDElementDAO {

    int TYPE_REAL = 0;
    int TYPE_INTEGER = 1;
    int TYPE_BOOLEAN = 2;

    @Insert
    void insert(BDElementEntity element);

    @Query("SELECT * FROM Element WHERE id IN (SELECT element FROM ElementStation WHERE station = :station)")
    List<BDElementEntity> getElementsOnStationNonLive(int station);

    @Query("SELECT * FROM Element ORDER BY id ASC")
    List<BDElementEntity> getAllElementsNonLive();

    @Query("SELECT * FROM Element WHERE id = :n")
    BDElementEntity getElementByIdNonLive(int n);

    @Query("SELECT count(id) FROM ELEMENT WHERE id = :n GROUP BY id")
    boolean elementExists(int n);
}
