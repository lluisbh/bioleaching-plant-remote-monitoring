package com.example.appbio;

import org.json.JSONException;
import org.json.JSONObject;

public class UserCommand {

    public static class UserCharacteristics {
        public final String username;
        public final String password;
        public final boolean admin;

        UserCharacteristics(String username, String password, boolean admin) {
            this.username = username;
            this.password = password;
            this.admin = admin;
        }
    }

    public static UserCharacteristics userAdd(String username, String password, boolean admin) {
        ServerRequest request = new ServerRequest(App.serverAddress + "users/", "POST");
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", username);
            if (!password.isEmpty()) {
                obj.put("password", password);
            }
            obj.put("admin", admin);

            request.setMessage(obj.toString());
            request.setContentType("application/json");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        String response = request.sendRequest();
        if (response != null) {
            try {
                JSONObject responseObj = new JSONObject(response);
                username = responseObj.getString("username");
                password = responseObj.getString("password");
                admin = responseObj.getBoolean("admin");

                //tornar nom, pass, admin
                return new UserCharacteristics(username, password, admin);

            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            //llegir error
            return null;
        }
    }

    public static boolean userChange(String username, String password) {
        ServerRequest request = new ServerRequest(App.serverAddress + "users/" + username, "PUT");
        JSONObject obj = new JSONObject();
        try {
            obj.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        request.setMessage(obj.toString());
        request.setContentType("application/json");

        request.sendRequest();

        return request.getCode() == 204;
    }

    public static boolean userRemove(String username) {
        ServerRequest request = new ServerRequest(App.serverAddress + "users/" + username, "DELETE");
        request.sendRequest();

        return request.getCode() == 204;
    }
}
