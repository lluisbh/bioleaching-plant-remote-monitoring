package com.example.appbio;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

@Entity(tableName = "Element")
public class BDElementEntity {

    @PrimaryKey
    private final int id;

    @NonNull
    private final String name;

    //Tipus de valor a guardar
    private final int valueType;

    // chart settings
    private final float maxVal;
    private final float minVal;

    // can act
    private final boolean canAct;

    public BDElementEntity(int id, @NonNull String name, int valueType, boolean canAct, float minVal, float maxVal) {
        this.id = id;
        this.name = name;
        this.valueType = valueType;
        this.maxVal = maxVal;
        this.minVal = minVal;
        this.canAct = canAct;
    }

    @Ignore
    public BDElementEntity(int id, @NonNull String name, int valueType, boolean canAct) {
        this.id = id;
        this.name = name;
        this.valueType = valueType;
        this.maxVal = -1;
        this.minVal = -1;
        this.canAct = canAct;
    }

    public int getId() {return id;}

    public String getDescription(Context context) {
        InputStream in = context.getResources().openRawResource(R.raw.components);
        try {
            return XMLComponentParser.getDescription(in, this.name);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getNameFinal(Context context) {
        InputStream in = context.getResources().openRawResource(R.raw.components);
        try {
            return XMLComponentParser.getName(in, this.name);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    public String getName() { return name; }

    public int getValueType() { return valueType; }

    public float getMaxVal() { return maxVal; }

    public float getMinVal() { return minVal; }

    public boolean canAct() { return canAct; }

    public String getUnitFinal(Context context) {
        //Rebre valor del fitxer traduït
        InputStream in = context.getResources().openRawResource(R.raw.components);
        try {
            return XMLComponentParser.getUnit(in, this.name);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
