package com.example.appbio;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BDRegisterDAO {
    @Insert
    void insert(BDRegisterEntity register);

    @Query("DELETE FROM Register")
    void deleteAll();

    @Query("DELETE FROM Register WHERE element = :element AND date IN " +
            "(SELECT date FROM Register WHERE date < :limit AND element = :element " +
            "ORDER BY date DESC LIMIT -1 OFFSET 1)")
    void delete_until(int element, long limit);

    @Query("SELECT * FROM Register AS reg WHERE element = :id AND ((date >= :from AND date <= :to) OR " +
            "(date = (SELECT date FROM Register WHERE date < :from AND element = reg.element " +
            "ORDER BY date DESC LIMIT 1))) ORDER BY date ASC")
    LiveData<List<BDRegisterEntity>> getElementRegistersFiltered(int id, long from, long to);

    @Query("SELECT * FROM Register WHERE element = :id ORDER BY date DESC LIMIT 1")
    LiveData<BDRegisterEntity> getElementLastRegister(int id);

    @Query("SELECT * FROM Register AS reg WHERE element = :id AND ((date >= :from AND date <= :to) OR " +
            "(date = (SELECT date FROM Register WHERE date < :from AND element = reg.element " +
            "ORDER BY date DESC LIMIT 1))) ORDER BY date ASC")
    BDRegisterEntity[] getElementRegistersFilteredNonLive(int id, long from, long to);

    @Query("SELECT * FROM Register as reg WHERE (date >= :from AND date <= :to) OR " +
            "(date = (SELECT date FROM Register WHERE date < :from AND element = reg.element " +
            "ORDER BY date DESC LIMIT 1))  ORDER BY date ASC")
    BDRegisterEntity[] getAllRegistersFiltered(long from, long to);
}
