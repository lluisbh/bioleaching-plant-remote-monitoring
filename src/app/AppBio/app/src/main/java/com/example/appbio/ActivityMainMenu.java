package com.example.appbio;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ActivityMainMenu extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_main_menu);
        super.onCreate(savedInstanceState);
    }

    public void onClickMenu(View view) {
        int b = view.getId();
        Intent intent = new Intent(this, ActivityStation.class);
        int stationId;

        if (b == R.id.button_bio) {
            stationId = 0;
        } else if (b == R.id.button_tank1) {
            stationId = 1;
        } else if (b == R.id.button_lix) {
            stationId = 2;
        } else if (b == R.id.button_tank2) {
            stationId = 4;
        } else if (b == R.id.button_rec) {
            stationId = 3;
        } else {
            stationId = 0;
        }

        intent.putExtra(ActivityStation.EXTRA_STATION, stationId);
        startActivity(intent);
    }
}