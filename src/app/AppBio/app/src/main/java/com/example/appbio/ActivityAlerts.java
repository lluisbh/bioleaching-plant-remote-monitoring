package com.example.appbio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LiveData;

import java.util.HashMap;
import java.util.Objects;

public class ActivityAlerts extends ActivityBase {
    LiveData<BDAlertEntity[]> alerts;
    int[] alertStation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_alerts);
        super.onCreate(savedInstanceState);

        Context context = this;
        BDRoom bdRoom = BDRoom.getDatabase(context);
        BDAlertDAO alertDAO = bdRoom.alertDAO();
        alerts = alertDAO.getAlerts();
        alerts.observe(this, bdAlertEntities -> {

            int len = bdAlertEntities.length;
            alertStation = new int[len];
            BDRoom.databaseWriteExecutor.execute(() -> {
                HashMap<Integer, BDElementEntity> elements = new HashMap<>();
                HashMap<Integer, BDStationEntity> stations = new HashMap<>();
                BDElementDAO elementDAO = bdRoom.elementDAO();
                BDStationDAO stationDAO = bdRoom.stationDAO();
                for (BDAlertEntity alert : bdAlertEntities) {
                    int elementId = alert.getElement();
                    if (elements.get(elementId) == null) {
                        BDElementEntity element = elementDAO.getElementByIdNonLive(elementId);
                        elements.put(elementId, element);
                        BDStationEntity station = stationDAO.getFirstStation(element.getId());
                        stations.put(elementId, station);
                    }
                }

                runOnUiThread(() -> {
                    LayoutInflater layoutInflater = getLayoutInflater();
                    LinearLayout linearLayout = findViewById(R.id.LinearLayoutUsers);
                    for (BDAlertEntity alert : bdAlertEntities) {
                        ConstraintLayout row = (ConstraintLayout) layoutInflater.inflate(
                                R.layout.alert_row, linearLayout, false);
                        ((TextView) row.getChildAt(1)).setText(getString(R.string.data_string_name,
                                getString(R.string.text_date), DateFormat.format
                                        ("dd/MM/yyyy HH:mm",alert.getDate() * 60000)
                                        .toString()));

                        BDElementEntity elementEntity = elements.get(alert.getElement());
                        assert elementEntity != null;
                        ((TextView) row.getChildAt(2)).setText(getString(R.string.data_string_name,
                                getString(R.string.text_component),
                                elementEntity.getNameFinal(context)));

                        BDStationEntity stationEntity = stations.get(alert.getElement());
                        assert stationEntity != null;
                        ((TextView) row.getChildAt(3)).setText(getString(R.string.data_string_name,
                                getString(R.string.text_station), stationEntity.getNameFinal(context)));

                        ((TextView) row.getChildAt(4)).setText(alert.getMessage());

                        linearLayout.addView(row);
                    }
                });

            });
        });
    }

    public void onClickAlert(View view) {
        View parent = (View) view.getParent();
        int index = ((LinearLayout) parent.getParent()).indexOfChild(parent);
        Intent intent = new Intent(this, ActivityStation.class);
        int station = alertStation[index];
        intent.putExtra(ActivityStation.EXTRA_STATION, station);
        startActivity(intent);
    }
}