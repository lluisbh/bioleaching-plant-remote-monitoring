package com.example.appbio;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "Register",
        primaryKeys = {"element", "date"},
        foreignKeys = {@ForeignKey(entity = BDElementEntity.class,
        parentColumns = "id",
        childColumns = "element"
        )},
        indices = {@Index(value = {"element"})})
public class BDRegisterEntity {

    private final int element;

    private final long date;

    private final float number;

    public BDRegisterEntity(int element, float number, long date) {
        this.element = element;
        this.date = date;
        this.number = number;
    }

    public int getElement() { return element; }

    public long getDate() { return date; }

    public float getNumber() { return number; }

}
