package com.example.appbio;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

@Entity(tableName = "ElementStation" ,
        primaryKeys = {"element", "station"},
        foreignKeys = {@ForeignKey(entity = BDElementEntity.class,
        parentColumns = "id",
        childColumns = "element"
        ),
        @ForeignKey(entity = BDStationEntity.class,
        parentColumns = "id",
        childColumns = "station"
        )},
        indices = {@Index(value = {"element"}), @Index(value = {"station"})}
)
public class BDElementStatonEntity {

    private final int element;
    private final int station;

    public BDElementStatonEntity(int element, int station) {
        this.element = element;
        this.station = station;
    }

    public int getElement() { return element; }

    public int getStation() { return station; }
}
