package com.example.appbio;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "Alert" , foreignKeys = {@ForeignKey(entity = BDElementEntity.class,
        parentColumns = "id",
        childColumns = "element"
        )},
        indices = {@Index(value = {"element"})}
)
public class BDAlertEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    private final long date;

    private final int element;

    @NonNull
    private final String message;

    public BDAlertEntity(long date, int element, @NonNull String message) {
        this.date = date;
        this.element = element;
        this.message = message;
    }

    public long getDate() { return date; }

    public int getElement() { return element; }

    @NonNull
    public String getMessage() { return message; }
}
