
package com.example.appbio;

import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.os.HandlerCompat;

import org.json.JSONException;
import org.json.JSONObject;

public class ActivityLogin extends AppCompatActivity {
    //Login page

    private final Handler mainThreadHandler = HandlerCompat.createAsync(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Set view
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        //Set toolbar
        setSupportActionBar(myToolbar);
    }

    public void sendLoginInfo(@SuppressWarnings("unused") View v) {
        //Get username from textbox
        EditText editTextUser = findViewById(R.id.editTextUsername);
        String username = editTextUser.getText().toString();
        //Get password from textbox
        EditText editTextPass = findViewById(R.id.editTextPassword);
        String password = editTextPass.getText().toString();
        //Send these to server, wait for response...
        ServerRequest request = new ServerRequest(App.serverAddress + "auth/login",
                "POST", "username=" + username + "&password=" + password);

        //...on a new thread
        Context context = this;
        ServerRequest.requestExecutor.execute(() -> {
            String response = request.sendRequest();
            if (response != null) {
                try {
                    JSONObject json = new JSONObject(response);
                    //login success
                    if (json.getBoolean("response")) {
                        SharedPreferencesManager sharedPreferencesManager =
                                new SharedPreferencesManager(context);
                        sharedPreferencesManager.setIsAdmin(json.getBoolean("admin"));
                        sharedPreferencesManager.setIsLoggedIn(true);
                        sharedPreferencesManager.apply();

                        //Començar a rebre dades
                        DataUpdateSchedule.startSchedule(getApplicationContext());

                        //Inicialitza l'alarma al reiniciar el dispositiu
                        ComponentName receiver = new ComponentName(context, BootReceiver.class);
                        PackageManager pm = context.getPackageManager();
                        pm.setComponentEnabledSetting(receiver,
                                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                PackageManager.DONT_KILL_APP);

                        //start new activities
                        mainThreadHandler.post(() -> {
                            DataRequest.startAlarm(context.getApplicationContext());
                            Intent intent = new Intent(context, ActivityMainMenu.class);
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                            stackBuilder.addNextIntent(intent);
                            stackBuilder.startActivities();
                        });
                    } else {
                        //Alert
                        //Nom o Contrasenya incorrectes
                        AlertErrorBuilder alertErrorBuilder = new AlertErrorBuilder(context,
                                R.string.error_wrong_name_pwd);
                        mainThreadHandler.post(alertErrorBuilder::show);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //No connection
                AlertErrorBuilder alertErrorBuilder = new AlertErrorBuilder(context,
                        R.string.error_command);
                mainThreadHandler.post(alertErrorBuilder::show);
            }
        });
    }

    //no deixar tornar a l'app
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}