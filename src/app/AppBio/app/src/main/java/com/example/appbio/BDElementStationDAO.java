package com.example.appbio;

import androidx.room.Dao;
import androidx.room.Insert;

@Dao
public interface BDElementStationDAO {
    @Insert
    void insert(BDElementStatonEntity elementStatonEntity);
}
