package com.example.appbio;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//exportSchema?
//migration strategy
//https://developer.android.com/codelabs/android-room-with-a-view
@Database(entities = {BDElementEntity.class, BDRegisterEntity.class,
        BDRangeEntity.class, BDStationEntity.class,
        BDAlertEntity.class, BDElementStatonEntity.class},
        version = 1, exportSchema = false)
public abstract class BDRoom extends RoomDatabase {
    public abstract BDElementDAO elementDAO();
    public abstract BDRegisterDAO registerDAO();
    public abstract BDRangeDAO rangeDAO();
    public abstract BDStationDAO stationDAO();
    public abstract BDAlertDAO alertDAO();
    public abstract BDElementStationDAO elementStationDAO();

    private static volatile BDRoom INSTANCE;
    private static final int NUM_THREADS = 2;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUM_THREADS);

    static BDRoom getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (BDRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            BDRoom.class, "bioapp_database").build();
                }
            }
        }
        return INSTANCE;
    }

    public static void initDB(Context context) {
        //Funció per inicialitzar la bbdd a partir d'un fitxer
        //Lector XML
        InputStream in = context.getResources().openRawResource(R.raw.stations_elements);
        List<XMLStationParser.Element> elements = null;
        try {
            elements = XMLStationParser.parse(in);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        if (elements != null) {
            BDRoom bdRoom = getDatabase(context);
            List<XMLStationParser.Element> finalElements = elements;
            databaseWriteExecutor.execute(() -> {

                BDStationDAO daoStation = bdRoom.stationDAO();
                BDElementDAO dao = bdRoom.elementDAO();

                List<BDElementStatonEntity> dupes = new ArrayList<>();

                for (XMLStationParser.Element e :
                        finalElements) {
                    switch (e.elementClass) {
                        case XMLStationParser.Element.CLASS_STATION:
                            //És estació
                            XMLStationParser.Station s = (XMLStationParser.Station) e;
                            BDStationEntity station = new BDStationEntity(s.id, s.name);
                            daoStation.insert(station);
                            break;
                        case XMLStationParser.Element.CLASS_COMPONENT:
                            //És component
                            XMLStationParser.Component c = (XMLStationParser.Component) e;
                            dupes.add(new BDElementStatonEntity(c.id, c.stationId));
                            switch (c.type) {
                                case XMLStationParser.Component.TYPE_REAL:
                                    //valor decimal
                                    dao.insert(new BDElementEntity(c.id, c.name, BDElementDAO.TYPE_REAL, c.canAct, c.min, c.max));
                                    break;
                                case XMLStationParser.Component.TYPE_INTEGER:
                                    //Valor enter
                                    dao.insert(new BDElementEntity(c.id, c.name, BDElementDAO.TYPE_INTEGER, c.canAct, c.min, c.max));
                                    break;
                                case XMLStationParser.Component.TYPE_BOOLEAN:
                                    //Boolean
                                    dao.insert(new BDElementEntity(c.id, c.name, BDElementDAO.TYPE_BOOLEAN, c.canAct));
                                    break;
                                case XMLStationParser.Component.TYPE_DUPLICATE:
                                    //Duplicat
                                    break;
                            }

                            break;
                        default:
                            break;
                    }
                }

                BDElementStationDAO elementStationDAO = bdRoom.elementStationDAO();

                for (BDElementStatonEntity e :
                        dupes) {
                    elementStationDAO.insert(e);
                }
            });
        }
    }
}
