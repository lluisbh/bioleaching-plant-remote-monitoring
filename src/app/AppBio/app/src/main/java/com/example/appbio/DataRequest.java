package com.example.appbio;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DataRequest {

    private static final ExecutorService executor = Executors.newFixedThreadPool(2);

    private static abstract class RunnableData implements Runnable {

        private boolean ended = false;
        private boolean success = false;

        public boolean isSuccessful() { return success; }

        public boolean isRunning() {
            return !ended;
        }

        protected void end(boolean success) {
            synchronized (this) {
                this.ended = true;
                this.success = success;
                this.notifyAll();
            }
        }
    }

    public static class RunnableGetAlerts extends RunnableData {

        private final Context context;
        private final long from;
        private final long to;

        public RunnableGetAlerts(Context context, long from, long to) {
            this.context = context;
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            String url = String.format(Locale.getDefault(),App.serverAddress + "data/alerts/?from=%d&to=%d", from, to);
            ServerRequest request = new ServerRequest(url, "GET");

            String response = request.sendRequest();
            if (response != null) {
                BDRoom.databaseWriteExecutor.execute(() -> {
                    BDRoom bdRoom = BDRoom.getDatabase(context);
                    BDAlertDAO alertDAO = bdRoom.alertDAO();
                    BDElementDAO elementDAO = bdRoom.elementDAO();
                    BDStationDAO stationDAO = bdRoom.stationDAO();
                    NotificationAlert notificationAlert = NotificationAlert.getInstance(context);
                    try {
                        JSONArray array = new JSONArray(response);
                        int len = array.length();
                        for (int i = 0; i < len; i++) {
                            JSONObject obj = (JSONObject) array.get(i);
                            int component = obj.getInt("Component");
                            long date = obj.getLong("Date");
                            String message = obj.getString("Message");

                            BDAlertEntity alertEntity = new BDAlertEntity(date, component, message);
                            alertDAO.insert(alertEntity);

                            //notification
                            BDElementEntity element = elementDAO.getElementByIdNonLive(component);
                            BDStationEntity station = stationDAO.getFirstStation(element.getId());
                            String elementName = element.getNameFinal(context);
                            String stationName = station.getNameFinal(context);
                            notificationAlert.SetAlert(context,
                                    context.getString(R.string.notification_alert_template, elementName,
                                            stationName, message), station.getId());
                        }
                        end(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        end(false);
                    }
                });
            } else if (request.getCode() == 401){
                App.logOut(context);
                end(false);
            }
        }
    }

    public static class RunnableGetData extends RunnableData {

        private final Context context;
        private final int id;
        private final long from;
        private final long to;

        public RunnableGetData(int id, long from, long to, Context context) {
            this.id = id;
            this.from = from;
            this.to = to;
            this.context = context;
        }

        @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
        @Override
        public void run() {

            if (id == -1) {
                //Agafar tots els elements
                String url = App.serverAddress + "data/";
                ServerRequest request = new ServerRequest(url, "GET");

                String response = request.sendRequest();
                if (response != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        int len = jsonArray.length();
                        //Legir index de dades disponibles
                        for (int i = 0; i < len; i++) {
                            JSONObject obj = (JSONObject) jsonArray.get(i);
                            //Trucada recursiva per rebre les dades de l'element actual
                            RunnableGetData thread = getData(from, to, obj.getInt("Component"), context);
                            synchronized (thread) {
                                if (thread.isRunning()) {
                                    thread.wait(5000);
                                }
                                if (!thread.isSuccessful()) {
                                    end(false);
                                    return;
                                }
                            }
                        }
                    } catch (JSONException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    end(true);
                } else if (request.getCode() == 401){
                    //Sense permisos
                    App.logOut(context);
                    end(false);
                } else {
                    //Altre error
                    end(false);
                }
            } else {
                BDRoom.databaseWriteExecutor.execute(() -> {
                    BDRoom bdRoom = BDRoom.getDatabase(context);
                    BDElementDAO elementDAO = bdRoom.elementDAO();
                    if (elementDAO.elementExists(id)) {
                        BDRangeDAO rangeDAO = bdRoom.rangeDAO();
                        if (rangeDAO.getRangeWith(id, from, to) == null) { // Aquest rang de dates no està ja guardada a la BBDD
                            long fromNew = from;
                            //no demanar dades més tard d'ara.
                            long toNew = to;
                            BDRangeEntity rangeEntityUp;
                            BDRangeEntity rangeEntityDown;
                            if ((rangeEntityUp = rangeDAO.getRangeWithUp(id, from)) != null) { //Aquest rang forma part d'un altre, buscar només part necessària
                                fromNew = rangeEntityUp.getDateTo() + 1;
                                rangeEntityUp.setDateTo(to);
                            }
                            if ((rangeEntityDown = rangeDAO.getRangeWithDown(id, to)) != null) {
                                toNew = rangeEntityDown.getDateFrom() - 1;
                                rangeEntityDown.setDateFrom(from);
                            }

                            long finalFromNew = fromNew;
                            long finalToNew = toNew;
                            executor.execute(() -> {
                                //Enviar petició
                                String url = String.format(Locale.getDefault(), App.serverAddress
                                        + "data/%d?from=%d&to=%d", id, finalFromNew, finalToNew);
                                ServerRequest request = new ServerRequest(url, "GET");
                                String response = request.sendRequest();
                                if (response != null) {
                                    try {
                                        storeToBD(new JSONArray(response), bdRoom);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    BDRoom.databaseWriteExecutor.execute(() -> {
                                        //Si no hi ha hagut problemes, actualitzar rangs
                                        if (rangeEntityUp != null && rangeEntityDown != null) {
                                            rangeEntityUp.setDateTo(rangeEntityDown.getDateTo());
                                            rangeDAO.delete(rangeEntityDown);
                                            rangeDAO.update(rangeEntityUp);
                                        } else if (rangeEntityUp != null) {
                                            rangeDAO.update(rangeEntityUp);
                                        } else if (rangeEntityDown != null) {
                                            rangeDAO.update(rangeEntityDown);
                                        } else {
                                            BDRangeEntity newRange = new BDRangeEntity(id, finalFromNew,
                                                    finalToNew);
                                            rangeDAO.insert(newRange);
                                        }
                                        //Eliminar rangs sobrants
                                        rangeDAO.deleteRange(id, finalFromNew, finalToNew);
                                        end(true);
                                    });
                                } else if (request.getCode() == 401) {
                                    //Sense permisos
                                    App.logOut(context);
                                    end(false);
                                } else {
                                    //Altre error
                                    end(false);
                                }
                            });
                        } else {
                            //Ja estan les dades
                            end(true);
                        }
                    } else {
                        end(true);//no ens fa falta rebre
                    }
                });
            }
        }
    }

    public static class RunnableGetUsers extends RunnableData {

        private String[] names;

        public String[] getNames() {
            return names;
        }

        @Override
        public void run() {
            String url = App.serverAddress + "users/";
            ServerRequest request = new ServerRequest(url, "GET");

            ServerRequest.requestExecutor.execute(() -> {
                String response = request.sendRequest();
                if (response != null) {
                    try {
                        JSONArray array = new JSONArray(response);
                        int len = array.length();
                        names = new String[len];
                        for (int i = 0; i < len; i++) {
                            JSONObject obj = array.getJSONObject(i);
                            names[i] = obj.getString("username");
                        }
                        end(true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        end(false);
                    }
                } else {
                    end(false);
                }
            });
        }
    }

    public static void startAlarm(Context context) {
        //Inicialitza petició de dades cada minut
        UpdateReceiver.startAlarm(context.getApplicationContext(), -1, -1);
    }

    public static RunnableGetData getData(long from, long to, int id, Context context) {
        RunnableGetData run = new RunnableGetData(id, from, to, context);
        executor.execute(run);
        return run;
    }

    private static void storeToBD(JSONArray json, BDRoom bdRoom) {
        BDRoom.databaseWriteExecutor.execute(() -> {
            BDRegisterDAO registerDAO = bdRoom.registerDAO();

            // Guardar a la BBDD
            int len = json.length();
            for (int i = 0; i < len; i++) {
                BDRegisterEntity register = null;
                try {
                    JSONObject obj = (JSONObject) json.get(i);
                    register = new BDRegisterEntity(obj.getInt("Component"),
                            (float) obj.getDouble("Value"), obj.getLong("Date"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (register != null) registerDAO.insert(register);
                } catch (SQLiteConstraintException e) {
                    //e.printStackTrace();
                    //Si ja es troba a la base de dades, ignorar
                }
            }
        });
    }

    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public static void getLast24hours(Context context, int id) {
        long timeNow = new Date().getTime() / 60000;
        long timeBefore = timeNow - 24 * 60 * 60;
        BDRoom.databaseWriteExecutor.execute( () -> {
            BDRoom bdRoom = BDRoom.getDatabase(context);
            BDRegisterDAO registerDAO = bdRoom.registerDAO();
            BDRangeDAO rangeDAO = bdRoom.rangeDAO();

            executor.execute( () -> {
                registerDAO.delete_until(id, timeBefore);

                DataRequest.RunnableGetData run = DataRequest.getData(timeBefore, timeNow, id, context);
                boolean success;
                synchronized (run) {
                    if (run.isRunning()) {
                        try {
                            run.wait(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    success = run.isSuccessful();
                }

                if (success) {
                    BDRoom.databaseWriteExecutor.execute(() -> {
                        rangeDAO.deleteAllElement(id);
                        rangeDAO.insert(new BDRangeEntity(id, timeBefore, timeNow));
                    });
                }
            });
        });
    }

    public static void resetData(Context context) {
        BDRoom.databaseWriteExecutor.execute( () -> {
            BDRoom bdRoom = BDRoom.getDatabase(context);
            BDRegisterDAO bdRegisterDAO = bdRoom.registerDAO();
            bdRegisterDAO.deleteAll();
            BDRangeDAO bdRangeDAO = bdRoom.rangeDAO();
            bdRangeDAO.deleteAll();
        });
    }

    public static RunnableGetAlerts getAlerts(Context context, long from, long to) {
        RunnableGetAlerts run = new RunnableGetAlerts(context, from, to);
        executor.execute(run);
        return run;
    }

    public static RunnableGetUsers getUsers() {
        RunnableGetUsers run = new RunnableGetUsers();
        executor.execute(run);
        return run;
    }
}
