package com.example.appbio;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerRequest {
    /*
     * Class to send requests to the server
     * Creates new HttpURLConnections with the same configuration each time
     * Can be used multiple times
     */

    public static final ExecutorService requestExecutor = Executors.newFixedThreadPool(2);

    private URL url;
    private String method;
    private String message = "";
    private String contentType;
    private int code = 0;
    private int message_length = 0;


    public void SetUrl(String address) {
        try {
            url = new URL(address);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void SetMethod(String m){
        method = m;
    }

    public void setMessage(String m) {
        message = m;
        message_length = message.length();// correct for UTF-8
    }

    // Constructors
    public ServerRequest(String address, String method) {
        SetUrl(address);
        SetMethod(method);
    }
    public ServerRequest(String address, String method, String message) {
        SetUrl(address);
        SetMethod(method);
        setMessage(message);
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String sendRequest() {
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(method);
            if (contentType != null) {
                con.setRequestProperty("content-type", contentType);
            }
            if (method.equals("POST") || method.equals("PUT")) {
                con.setDoOutput(true);
                //con.setChunkedStreamingMode(0);
                con.setFixedLengthStreamingMode(message_length);
                OutputStreamWriter outputWrite =
                        new OutputStreamWriter( new BufferedOutputStream(con.getOutputStream()) );
                outputWrite.write(message);
                outputWrite.flush();
            }

            code = con.getResponseCode();

            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }

            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("error code", String.valueOf(code));
            return null;
        }
    }

    public int getCode(){
        return code;
    }
}
