package com.example.appbio;

import android.content.Context;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;

import androidx.core.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class StationView extends androidx.appcompat.widget.AppCompatImageView {

    private LayerDrawable drawable;
    private final Map<Integer, Integer> mapLevels = new HashMap<>();

    private final Map<Integer, Pair<Integer, Integer>> mapIdColors = new HashMap<>();
    private final Map<Integer, ColorGroup> mapElementColor = new HashMap<>();

    public static final int COLOR_RED = 0;
    public static final int COLOR_BLUE = 1;
    public static final int COLOR_GREEN = 2;

    private static class ColorGroup {
        public int red = 0;
        public int green = 0;
        public int blue = 0;
    }

    public StationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        drawable = (LayerDrawable) getDrawable();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        drawable = (LayerDrawable) getDrawable();
    }

    public void setRelationshipLevel(int id, int element) {

        mapLevels.put(id, element+1);
    }

    public void setRelationshipColor(int id, int element, int color) {
        Pair<Integer, Integer> colorTuple = new Pair<>(element+1, color);
        mapIdColors.put(id, colorTuple);
        mapElementColor.put(element+1, new ColorGroup());
    }

    public void setLevel(int id, int level) {
        Integer index = mapLevels.get(id);
        if (index != null) {
            drawable.getDrawable(index).setLevel(level);
        }
    }

    public void setColor(int id, int value) {
        Pair<Integer, Integer> elementColor = mapIdColors.get(id);
        if (elementColor != null) {
            int element = elementColor.first;
            int color = elementColor.second;
            ColorGroup colorGroup = mapElementColor.get(element);
            assert colorGroup != null;
            switch (color) {
                case COLOR_RED:
                    colorGroup.red = value;
                    break;
                case COLOR_GREEN:
                    colorGroup.green = value;
                    break;
                case COLOR_BLUE:
                    colorGroup.blue = value;
                    break;
            }
        }
    }

    public void updateColors() {
        for (Map.Entry<Integer, ColorGroup> m:
            mapElementColor.entrySet()) {
            ColorGroup colorGroup = m.getValue();
            int color = Color.BLACK | (colorGroup.red << 16) | (colorGroup.green << 8) | colorGroup.blue;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                BlendModeColorFilter colorFilter = new BlendModeColorFilter(color, BlendMode.MULTIPLY);
                drawable.getDrawable(m.getKey()).setColorFilter(colorFilter);
            } else {
                drawable.getDrawable(m.getKey()).setColorFilter(color, PorterDuff.Mode.MULTIPLY);
            }
        }
    }
}
