package com.example.appbio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import java.util.ArrayList;
import java.util.List;

public class ActivityStation extends ActivityBase {

    private int stationId;

    ArrayList<LiveData<BDRegisterEntity>> lastRegister;
    int[] elementsId;

    public static final String EXTRA_STATION = "com.example.appbio.STATION";

    private final class ObserverElement implements Observer<BDRegisterEntity> {
        private final TextView textView;
        private final String unit;

        public static final int TYPE_REAL = 0;
        public static final int TYPE_INTEGER = 1;
        public static final int TYPE_BOOLEAN = 2;

        private final int type;

        @Override
        public void onChanged(BDRegisterEntity registerEntity) {
            //Actualitzar últim valor per cada element
            if (registerEntity != null) {
                StationView stationView = findViewById(R.id.stationView);
                float number = registerEntity.getNumber();
                String text;
                if (type == TYPE_BOOLEAN) {
                    if (number != 0) {
                        stationView.setLevel(registerEntity.getElement(), 1);
                        text = "ON";
                    } else {
                        stationView.setLevel(registerEntity.getElement(), 0);
                        text = "OFF";
                    }
                } else {
                    float v = registerEntity.getNumber();
                    if (type == TYPE_REAL) {
                        text = getString(R.string.data_value_real_unit, v, unit);
                    } else {//if (type == TYPE_INTEGER) {
                        text = getString(R.string.data_value_int_unit, (int) v, unit);
                    }
                    //per bombes
                    if (v > 0) {
                        stationView.setLevel(registerEntity.getElement(), 1);
                    } else if (v > 100) {
                        stationView.setLevel(registerEntity.getElement(), 2);
                    } else {
                        stationView.setLevel(registerEntity.getElement(), 0);
                    }
                    //per colors
                    stationView.setColor(registerEntity.getElement(), (int) v);
                    stationView.updateColors();
                }
                textView.setText(text);
            }
        }

        public ObserverElement(TextView text, String unit, int type) {
            this.textView = text;
            this.type = type;
            this.unit = unit;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.setActivityLayout(R.layout.activity_station);
        super.onCreate(savedInstanceState);

        setId();

        BDRoom bdRoom = BDRoom.getDatabase(this);
        BDElementDAO elementDAO = bdRoom.elementDAO();
        BDRegisterDAO registerDAO = bdRoom.registerDAO();

        Context context = this;

        //Carrega els elements a mostrar
        BDRoom.databaseWriteExecutor.execute(() -> {
            List<BDElementEntity> elements = elementDAO.getElementsOnStationNonLive(stationId);
            int len = elements.size();
            lastRegister = new ArrayList<>();
            elementsId = new int[len];

            int[] types = new int[len];
            String[] units = new String[len];

            for (int i = 0; i < len; i++) {
                BDElementEntity element = elements.get(i);
                elementsId[i] = element.getId();
                //Si aquest element treballa amb ON/OFF
                switch (element.getValueType()) {
                    case BDElementDAO.TYPE_BOOLEAN:
                        types[i] = ObserverElement.TYPE_BOOLEAN;
                        break;
                    case BDElementDAO.TYPE_INTEGER:
                        types[i] = ObserverElement.TYPE_INTEGER;
                        break;
                    //case BDElementDAO.TYPE_REAL:
                    default:
                        types[i] = ObserverElement.TYPE_REAL;
                        break;
                }
                units[i] = element.getUnitFinal(context);
            }

            runOnUiThread(() -> {
                //Carrega botó per cada element
                LayoutInflater layoutInflater = getLayoutInflater();
                ViewGroup list = findViewById(R.id.ListStation);

                for (int i = 0; i < len; i++) {
                    ViewGroup row = (ViewGroup) layoutInflater.inflate(R.layout.station_row, list, false);
                    list.addView(row);
                    BDElementEntity element = elements.get(i);

                    ((TextView) row.getChildAt(1)).setText(element.getNameFinal(context));

                    lastRegister.add(registerDAO.getElementLastRegister(element.getId()));
                    //Posa observer per cada element per actualitzar cada última dada
                    lastRegister.get(i).observe((LifecycleOwner) context, new ObserverElement((TextView) row.getChildAt(2), units[i], types[i]));
                }
            });
        });
    }

    public void onClickMenu(View view) {
        //Veure element
        View parent = (View) view.getParent();
        int index = ((ViewGroup) parent.getParent()).indexOfChild(parent);
        Intent intent = new Intent(this, ActivityElement.class);
        intent.putExtra(ActivityElement.EXTRA_ELEMENT, elementsId[index]);
        startActivity(intent);
    }

    private void setId() {
        //Configurar variables sobre quina estació ens trobem
        stationId = getIntent().getIntExtra(EXTRA_STATION, 0);
        StationView stationView = findViewById(R.id.stationView);
        TextView textName = findViewById(R.id.textStationName);
        // Configurar adequadament cada
        switch (stationId) {
            case 0:
                stationView.setImageResource(R.drawable.graphic_bio);
                stationView.setRelationshipLevel(10, 0);
                stationView.setRelationshipLevel(9, 1);
                stationView.setRelationshipLevel(7, 2);
                stationView.setRelationshipLevel(8, 3);
                stationView.setRelationshipLevel(5, 4);
                stationView.setRelationshipLevel(11, 5);
                stationView.setRelationshipLevel(34, 6);
                textName.setText(R.string.text_bioreactor);
                break;
            case 1:
                stationView.setImageResource(R.drawable.graphic_tank1);
                stationView.setRelationshipLevel(28, 0);
                stationView.setRelationshipLevel(29, 1);
                stationView.setRelationshipLevel(16, 2);
                stationView.setRelationshipLevel(11, 3);
                stationView.setRelationshipLevel(30, 4);
                textName.setText(R.string.text_tank_1);
                break;
            case 2:
                stationView.setImageResource(R.drawable.graphic_lix);
                stationView.setRelationshipLevel(14, 0);
                stationView.setRelationshipLevel(15, 1);
                stationView.setRelationshipLevel(16, 2);
                stationView.setRelationshipLevel(17, 3);
                stationView.setRelationshipLevel(18, 4);
                stationView.setRelationshipLevel(19, 5);
                stationView.setRelationshipLevel(20, 6);
                stationView.setRelationshipColor(36, 7, StationView.COLOR_RED);
                stationView.setRelationshipColor(37, 7, StationView.COLOR_GREEN);
                stationView.setRelationshipColor(38, 7, StationView.COLOR_BLUE);
                textName.setText(R.string.text_lix);
                break;
            case 3:
                stationView.setImageResource(R.drawable.graphic_rec);
                stationView.setRelationshipLevel(27, 0);
                stationView.setRelationshipLevel(26, 1);
                stationView.setRelationshipLevel(14, 2);
                stationView.setRelationshipLevel(24, 3);
                stationView.setRelationshipLevel(25, 4);
                stationView.setRelationshipColor(39, 5, StationView.COLOR_RED);
                stationView.setRelationshipColor(40, 5, StationView.COLOR_GREEN);
                stationView.setRelationshipColor(41, 5, StationView.COLOR_BLUE);
                textName.setText(R.string.text_recup);
                break;
            case 4:
                stationView.setImageResource(R.drawable.graphic_tank2);
                stationView.setRelationshipLevel(31, 0);
                stationView.setRelationshipLevel(32, 1);
                stationView.setRelationshipLevel(33, 2);
                stationView.setRelationshipLevel(34, 3);
                stationView.setRelationshipLevel(35, 4);
                stationView.setRelationshipLevel(30, 5);
                stationView.setRelationshipLevel(27, 6);
                textName.setText(R.string.text_tank_2);
                break;
        }
    }
}