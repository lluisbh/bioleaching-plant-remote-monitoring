package com.example.appbio;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface BDStationDAO {
    @Insert
    void insert(BDStationEntity station);

    @Query("SELECT * FROM Station WHERE id IN (SELECT station FROM ElementStation WHERE element = :element) LIMIT 1")
    BDStationEntity getFirstStation(int element);

}
