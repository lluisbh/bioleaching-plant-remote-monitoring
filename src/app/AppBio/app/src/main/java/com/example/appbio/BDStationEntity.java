package com.example.appbio;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

@Entity(tableName = "Station")
public class BDStationEntity {

    @PrimaryKey
    public final int id;

    @NonNull
    private final String name;

    @NonNull
    public String getName() { return name; }

    public String getNameFinal(Context context) {
        InputStream in = context.getResources().openRawResource(R.raw.components);
        try {
            return XMLComponentParser.getName(in, this.name);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getId() { return id; }

    public BDStationEntity(int id, @NonNull String name) {
        this.id = id;
        this.name = name;
    }
}
