package com.example.appbio;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Map;

public class AdapterData extends RecyclerView.Adapter<AdapterData.ViewHolder> {
//Classe d'adapter per la pàgina de dades

    private final BDRegisterEntity[] registers;
    private final Map<Integer, String> elementNames;
    private final boolean booleanResult;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textComponent;
        private final TextView textDate;
        private final TextView textValue;

        public ViewHolder(View view) {
            super(view);
            LinearLayout layout = (LinearLayout) view;

            textComponent = (TextView) layout.getChildAt(0);
            textDate = (TextView) layout.getChildAt(1);
            textValue = (TextView) layout.getChildAt(2);
        }

        public TextView getTextComponent() { return textComponent; }

        public TextView getTextDate() { return textDate; }

        public TextView getTextValue() { return textValue; }
    }

    public AdapterData(BDRegisterEntity[] dataSet, Map<Integer, String> names, boolean booleanResult) {
        this.registers = dataSet;
        this.elementNames = names;
        this.booleanResult = booleanResult;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.data_row, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        BDRegisterEntity register = registers[position];
        viewHolder.getTextComponent().setText(elementNames.get(register.getElement()));
        viewHolder.getTextDate().setText(DateFormat.format("dd/MM/yyyy HH:mm", register.getDate()*60000).toString());
        String value;
        if (booleanResult) {
            if (register.getNumber() != 0) {
                value = "ON";
            } else {
                value = "OFF";
            }
        } else {
            value = String.valueOf(register.getNumber());
        }
        viewHolder.getTextValue().setText(value);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return registers.length;
    }
}