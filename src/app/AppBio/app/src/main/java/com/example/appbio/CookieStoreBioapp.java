package com.example.appbio;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class CookieStoreBioapp implements CookieStore {
    private final SharedPreferences sharedpreferences;

    //constructor
    public CookieStoreBioapp(SharedPreferences preferences) {
        super();
        sharedpreferences = preferences;
    }

    @Override
    public void add(URI uri, HttpCookie cookie) {
        //Guardar cookies a shared preferences
        String stringJson = sharedpreferences.getString("main_cookie", "[]");
        try {
            JSONArray jsonArray = new JSONArray(stringJson);
            JSONObject obj = null;
            int end = jsonArray.length();
            int i;
            for (i=0; i < end; i++){
                obj = (JSONObject) jsonArray.get(i);
                if (obj.getString("uri").equals(uri.getAuthority())) {
                    break;
                }
            }
            if (obj == null) {
                obj = new JSONObject();
                obj.put("uri", uri.getAuthority());
                obj.put("cookies", new JSONArray());
            }

            JSONArray cookiesArray = obj.getJSONArray("cookies");
            String cookieString = cookie.toString();
            int cookiePrefixIndex = cookieString.indexOf("=");
            if (cookiePrefixIndex != -1) {
                String cookiePrefix = cookieString.substring( 0, cookiePrefixIndex);
                end = cookiesArray.length();
                for (int j = 0; j < end; j++) {
                    String s = cookiesArray.getString(j);
                    int indexEquals = s.indexOf("=");
                    if (indexEquals != -1) {
                        String prefix = s.substring( 0, indexEquals);
                        if (prefix.equals(cookiePrefix)) {
                            //Si existeix cookie amb mateix prefix, borrar anterior.
                            if (cookiesArray.remove(j) != null) {
                                end--;
                                j--;
                            }
                        }
                    }
                }
            }
            cookiesArray.put(cookieString);
            jsonArray.put(i, obj);

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("main_cookie", jsonArray.toString());
            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<HttpCookie> get(URI uri) {
        String stringJson = sharedpreferences.getString("main_cookie", "[]");
        List<HttpCookie> listReturn = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(stringJson);
            JSONObject obj;
            int end = jsonArray.length();
            for (int i=0; i < end; i++){
                obj = (JSONObject) jsonArray.get(i);
                if (obj.getString("uri").equals(uri.getAuthority())) {
                    JSONArray cookiesArray = obj.getJSONArray("cookies");
                    int end_j = cookiesArray.length();
                    for (int j = 0; j < end_j; j++) {
                        HttpCookie cookie = HttpCookie.parse((String) cookiesArray.get(j)).get(0);
                        listReturn.add(cookie);
                    }
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listReturn;
    }

    @Override
    public List<HttpCookie> getCookies() {
        return null;
    }

    @Override
    public List<URI> getURIs() {
        return null;
    }

    @Override
    public boolean remove(URI uri, HttpCookie cookie) {
        String stringJson = sharedpreferences.getString("main_cookie", "[]");
        try {
            JSONArray jsonArray = new JSONArray(stringJson);
            int end = jsonArray.length();
            int i;
            String uriAuth = uri.getAuthority();
            for (i=0; i < end; i++){

                JSONObject obj = (JSONObject) jsonArray.get(i);
                if (obj.getString("uri").equals(uriAuth)) {

                    JSONArray objArray = obj.getJSONArray("cookies");
                    end = objArray.length();
                    String cookieString = cookie.toString();
                    for (int j=0; j < end; j++) {

                        String jsonCookie = (String) objArray.get(j);
                        if (jsonCookie.equals(cookieString)) {
                            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                objArray.remove(j);
                            /*} else {
                                JSONArray newArray = new JSONArray();
                                for (int k = 0; k < j; k++) {
                                    newArray.put(k, objArray.get(k));
                                }
                                for (int k = j+1; k < end; k++) {
                                    newArray.put(k-1, objArray.get(k));
                                }
                                obj.put("cookies", newArray);
                            }*/
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("main_cookie", jsonArray.toString());
                            editor.apply();
                            return true;
                        }
                    }
                    return false;

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean removeAll() {
        return false;
    }
}
